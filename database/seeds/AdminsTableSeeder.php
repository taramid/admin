<?php

use Illuminate\Database\Seeder;
use Autocarat\Core\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'hanzo',
            'role' => Admin::ROLE_ADMIN,
            'email' => 'hanzo@gmail.com',
            'password' => bcrypt('123123'),
        ]);

        DB::table('admins')->insert([
            'name' => 'genji',
            'role' => Admin::ROLE_ADMIN,
            'note' => 'role - damage',
            'email' => 'genji@gmail.com',
            'password' => bcrypt('123123'),
        ]);

        DB::table('admins')->insert([
            'name' => 'mercy',
            'role' => Admin::ROLE_MANAGER,
            'note' => 'role - support',
            'email' => 'mercy@gmail.com',
            'password' => bcrypt('123123'),
        ]);

        DB::table('admins')->insert([
            'name' => 'd.va',
            'role' => Admin::ROLE_MANAGER,
            'note' => 'role - tank',
            'email' => 'd.va@gmail.com',
            'password' => bcrypt('123123'),
        ]);

        DB::table('admins')->insert([
            'name' => 'ema',
            'role' => Admin::ROLE_MANAGER,
            'email' => 'ema@gmail.com',
            'password' => bcrypt('123123'),
        ]);
    }
}
