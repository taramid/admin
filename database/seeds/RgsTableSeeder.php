<?php

use Illuminate\Database\Seeder;

class RgsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rgs')->insert([
            'supply_id' => 1,
            'name' => '10',
            'percent' => -8.5,
        ]);

        DB::table('rgs')->insert([
            'supply_id' => 1,
            'name' => '13',
            'percent' => -13,
        ]);

        DB::table('rgs')->insert([
            'supply_id' => 1,
            'name' => '16',
            'percent' => -20,
        ]);

        DB::table('rgs')->insert([
            'supply_id' => 1,
            'name' => '18',
            'percent' => -20,
        ]);


        DB::table('rgs')->insert([
            'supply_id' => 2,
            'name' => '21',
            'percent' => -21,
        ]);

        DB::table('rgs')->insert([
            'supply_id' => 2,
            'name' => '22',
            'percent' => -22,
        ]);

        DB::table('rgs')->insert([
            'supply_id' => 2,
            'name' => '23',
            'percent' => -23,
        ]);
    }
}
