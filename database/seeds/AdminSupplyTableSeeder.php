<?php

use Illuminate\Database\Seeder;

class AdminSupplyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_supply')->insert([
            'admin_id' => 1,
            'supply_id' => 1
        ]);

        DB::table('admin_supply')->insert([
            'admin_id' => 1,
            'supply_id' => 2
        ]);

        DB::table('admin_supply')->insert([
            'admin_id' => 2,
            'supply_id' => 1
        ]);
    }
}
