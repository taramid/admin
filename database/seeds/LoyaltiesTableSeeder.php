<?php

use Illuminate\Database\Seeder;

class LoyaltiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('loyalties')->insert([
            'supply_id' => 1,
            'name' => 'a',
        ]);

        DB::table('loyalties')->insert([
            'supply_id' => 1,
            'name' => 'b',
        ]);

        DB::table('loyalties')->insert([
            'supply_id' => 1,
            'name' => 'c',
        ]);

        DB::table('loyalties')->insert([
            'supply_id' => 2,
            'name' => 'k',
        ]);

        DB::table('loyalties')->insert([
            'supply_id' => 2,
            'name' => 'l',
        ]);

        DB::table('loyalties')->insert([
            'supply_id' => 2,
            'name' => 'm',
        ]);

        DB::table('loyalties')->insert([
            'supply_id' => 2,
            'name' => 'n',
        ]);

    }
}
