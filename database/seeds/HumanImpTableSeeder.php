<?php

use Illuminate\Database\Seeder;

class HumanImpTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('human_imp')->insert([
            'human_id' => 1,
            'imp_id' => 1
        ]);

        DB::table('human_imp')->insert([
            'human_id' => 1,
            'imp_id' => 2
        ]);

        DB::table('human_imp')->insert([
            'human_id' => 3,
            'imp_id' => 3
        ]);

        DB::table('human_imp')->insert([
            'human_id' => 4,
            'imp_id' => 3
        ]);


    }
}
