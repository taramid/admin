<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(ManufacturersTableSeeder::class);
        $this->call(SuppliersTableSeeder::class);
        $this->call(SuppliesTableSeeder::class);

        $this->call(AdminSupplyTableSeeder::class);

        $this->call(RgsTableSeeder::class);
        $this->call(LoyaltiesTableSeeder::class);
        $this->call(DiscountsTableSeeder::class);

        $this->call(ImpsTableSeeder::class);
        $this->call(HumansTableSeeder::class);
        $this->call(HumanImpTableSeeder::class);

        $this->call(MinionsTableSeeder::class);
    }
}
