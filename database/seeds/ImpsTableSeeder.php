<?php

use Illuminate\Database\Seeder;

class ImpsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('imps')->insert([
            'code' => 'U02',
            'note' => 'oh, well'
        ]);

        DB::table('imps')->insert([
            'code' => 'U08'
        ]);

        DB::table('imps')->insert([
            'code' => 'B12'
        ]);

        DB::table('imps')->insert([
            'code' => 'B14'
        ]);

        DB::table('imps')->insert([
            'code' => 'S6'
        ]);

        DB::table('imps')->insert([
            'code' => 'S8'
        ]);
    }
}
