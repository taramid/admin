<?php

use Illuminate\Database\Seeder;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suppliers')->insert([
            'name' => 'Berlin Auto House'
        ]);

        DB::table('suppliers')->insert([
            'name' => 'Germany AG'
        ]);

        DB::table('suppliers')->insert([
            'name' => 'EFA'
        ]);
    }
}
