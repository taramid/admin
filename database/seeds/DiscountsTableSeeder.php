<?php

use Illuminate\Database\Seeder;

class DiscountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discounts')->insert([
            'rg_id' => 1,
            'loyalty_id' => 1,
            'percent' => -4.5,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 1,
            'loyalty_id' => 2,
            'percent' => -5.5,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 2,
            'loyalty_id' => 1,
            'percent' => -9,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 2,
            'loyalty_id' => 2,
            'percent' => -9.5,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 2,
            'loyalty_id' => 3,
            'percent' => -10.5,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 3,
            'loyalty_id' => 1,
            'percent' => -16,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 3,
            'loyalty_id' => 2,
            'percent' => -17,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 3,
            'loyalty_id' => 3,
            'percent' => -18,
        ]);



        DB::table('discounts')->insert([
            'rg_id' => 5,
            'loyalty_id' => 4,
            'percent' => -21,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 5,
            'loyalty_id' => 5,
            'percent' => -22,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 5,
            'loyalty_id' => 6,
            'percent' => -23,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 5,
            'loyalty_id' => 7,
            'percent' => -23.5,
        ]);


        DB::table('discounts')->insert([
            'rg_id' => 6,
            'loyalty_id' => 4,
            'percent' => -22,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 6,
            'loyalty_id' => 5,
            'percent' => -23,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 6,
            'loyalty_id' => 6,
            'percent' => -24,
        ]);

        DB::table('discounts')->insert([
            'rg_id' => 6,
            'loyalty_id' => 7,
            'percent' => -24.5,
        ]);

    }
}
