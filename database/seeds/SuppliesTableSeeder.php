<?php

use Illuminate\Database\Seeder;

class SuppliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('supplies')->insert([
            'manufacturer_id' => 1,
            'supplier_id' => 1,
            'name' => 'MB',
            'alias' => 'MB'
        ]);

        DB::table('supplies')->insert([
            'manufacturer_id' => 2,
            'supplier_id' => 2,
            'name' => 'BMW',
            'alias' => 'BMW'
        ]);

        DB::table('supplies')->insert([
            'manufacturer_id' => 2,
            'supplier_id' => 2,
            'name' => 'BMW-mini',
            'alias' => 'BMW-mini'
        ]);

        DB::table('supplies')->insert([
            'manufacturer_id' => 2,
            'supplier_id' => 2,
            'name' => 'BMW-i',
            'alias' => 'BMW-i'
        ]);

        DB::table('supplies')->insert([
            'manufacturer_id' => 3,
            'supplier_id' => 2,
            'name' => 'VAG',
            'alias' => 'Volkswagen AG'
        ]);

        DB::table('supplies')->insert([
            'manufacturer_id' => 4,
            'supplier_id' => 2,
            'name' => 'LR',
            'alias' => 'LAND-ROVER'
        ]);
    }
}
