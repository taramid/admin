<?php

use Illuminate\Database\Seeder;

class HumansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('humans')->insert([
            'name' => 'Serhio',
            'note' => '1st',
            'email' => 'ss@gmail.com'
        ]);

        DB::table('humans')->insert([
            'Name' => 'Jane Doe',
            'note' => '2nd',
            'email' => 'jane@gmail.com'
        ]);

        DB::table('humans')->insert([
            'Name' => 'Leo',
            'email' => 'leo@gmail.com'
        ]);

        DB::table('humans')->insert([
            'Name' => 'Nickola',
            'note' => 'he-he',
            'email' => 'nickola@gmail.com'
        ]);

        DB::table('humans')->insert([
            'Name' => 'Stefano',
            'email' => 'stefano@gmail.com'
        ]);

        DB::table('humans')->insert([
            'Name' => 'batman',
            'email' => 'batman@gmail.com'
        ]);

        factory(Autocarat\Core\Human::class, 100)->create();
    }
}
