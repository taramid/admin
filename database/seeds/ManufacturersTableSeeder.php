<?php

use Illuminate\Database\Seeder;

class ManufacturersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('manufacturers')->insert([
            'name' => 'MB'
        ]);

        DB::table('manufacturers')->insert([
            'name' => 'BMW'
        ]);

        DB::table('manufacturers')->insert([
            'name' => 'VAG'
        ]);

        DB::table('manufacturers')->insert([
            'name' => 'LAND ROVER'
        ]);

        DB::table('manufacturers')->insert([
            'name' => 'KIA'
        ]);
    }
}
