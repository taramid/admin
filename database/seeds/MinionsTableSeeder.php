<?php

use Illuminate\Database\Seeder;
use Autocarat\Core\Minion;

class MinionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('minions')->insert([
            'name' => 'ANNA',
            'code' => 'AGE',
        ]);

        DB::table('minions')->insert([
            'name' => 'INGA',
            'code' => 'ING',
        ]);

        DB::table('minions')->insert([
            'name' => 'ALEX',
            'code' => 'ALX',
        ]);

        DB::table('minions')->insert([
            'name' => 'MAREK',
            'code' => 'MAR',
        ]);
    }
}
