<?php

use Faker\Generator as Faker;

$factory->define(Autocarat\Core\Human::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'note' => $faker->realText(32),
        'email' => $faker->unique()->safeEmail,
    ];
});
