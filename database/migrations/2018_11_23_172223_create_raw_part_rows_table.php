<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawPartRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raw_part_rows', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('catalog_import_id');
            $table->string('part_number', 42)->nullable();
            $table->string('cent_price', 13)->nullable();
            $table->string('rg', 6)->nullable();
            $table->string('warning', 42)->nullable();
            $table->string('cent_pfand', 13)->nullable();
            $table->string('weight', 11)->nullable();
            $table->string('teileart', 6)->nullable();
            $table->boolean('imported')->nullable();

            $table->foreign('catalog_import_id')
                ->references('id')->on('catalog_imports')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_part_rows');
    }
}
