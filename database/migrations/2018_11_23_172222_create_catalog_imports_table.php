<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('supply_id');
            $table->unsignedInteger('upload_id');
            $table->tinyInteger('step');
            $table->float('raw_read_time_taken')->nullable();
            $table->float('raw_read_memory_peak')->nullable();
            $table->unsignedInteger('raw_read_lines')->nullable();
            $table->float('import_time_taken')->nullable();
            $table->float('import_memory_peak')->nullable();
            $table->unsignedMediumInteger('import_rg_created')->nullable();
            $table->unsignedInteger('import_parts')->nullable();
            $table->timestamps();

            $table->foreign('supply_id')
                ->references('id')->on('supplies')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('upload_id')
                ->references('id')->on('uploads')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            // above 'restrict' cuz better first stop all catalog import queues etc
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_imports');
    }
}
