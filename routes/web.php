<?php

use App\Http\Controllers\DiscountController;

// auth
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// todo: GET request to 'logout' route causes MethodNotAllowedHttpException
// Search for "POST to Logout" - https://laravel.com/docs/5.3/upgrade

// password reset
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


Route::middleware(['auth:web'])->group(function () {

    Route::get('/', function () {
        return view('home');
    });

    Route::get('supplies/{supply}/catalog_imports', 'CatalogImportController@index')->name('catalog_imports.index');
    Route::post('supplies/{supply}/catalog_imports/upload', 'CatalogImportController@upload')->name('catalog_imports.upload');
    Route::get('supplies/{supply}/catalog_imports/preview/{import}', 'CatalogImportController@preview')->name('catalog_imports.preview');
    Route::post('supplies/{supply}/catalog_imports/apply/{import}', 'CatalogImportController@apply')->name('catalog_imports.apply');
    Route::delete('supplies/{supply}/catalog_imports/{import}', 'CatalogImportController@destroy')->name('catalog_imports.destroy');

    Route::get('supplies/{supply}/discounts', 'DiscountController@index')->name('discounts.index');
    Route::get('supplies/{supply}/discounts/export', [DiscountController::class, 'export'])->name('discounts.export');
    Route::post('supplies/{supply}/discounts/upload', [DiscountController::class, 'upload'])->name('discounts.upload');
    Route::get('supplies/{supply}/discounts/preview', [DiscountController::class, 'preview'])->name('discounts.preview');
    Route::post('supplies/{supply}/discounts/apply', [DiscountController::class, 'apply'])->name('discounts.apply');
    Route::patch('supplies/{supply}/discounts/{discount}', 'DiscountController@update')->name('discounts.update');
    Route::delete('supplies/{supply}/discounts/{discount}', 'DiscountController@destroy')->name('discounts.destroy');

    Route::resources([
        'admins' => 'AdminController',
        'manufacturers' => 'ManufacturerController',
        'suppliers' => 'SupplierController',
        'supplies' => 'SupplyController',
        'parts' => 'PartController',
        'imps' => 'ImpController',
        'humans' => 'HumanController',
        'minions' => 'MinionController',
    ]);
});
