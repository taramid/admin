<?php

namespace App\Jobs\CatalogImporting;

use App\CatalogImport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class RawRead implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    /**
     * Create a new job instance.
     *
     * @param  \App\CatalogImport  $catalogImport
     * @return void
     */
    public function __construct(CatalogImport $catalogImport)
    {
        $this->import = $catalogImport;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $startedAt = microtime(true);

        if (CatalogImport::STEP_UPLOADED === $this->import->step ) {
            //
            $pdo = DB::connection()->getPdo();
            //
            foreach($this->import->upload->files as $file) {
                $pdo->exec(<<<SQL
                    LOAD DATA LOCAL INFILE {$pdo->quote(storage_path('app/' . $file->path))}
                    INTO TABLE raw_part_rows
                    COLUMNS TERMINATED BY ';'
                    LINES TERMINATED BY '\n'
                    IGNORE 1 LINES
                    (
                        @part_number,
                        @euro_price,
                        @rg,
                        @warning,
                        @euro_pfand,
                        @weight,
                        @teileart
                    )
                    SET
                        catalog_import_id = {$this->import->id},
                        part_number = NULLIF(UPPER(TRIM(TRIM(BOTH '\r' FROM @part_number))), ''),
                        cent_price = NULLIF(100 * REPLACE(@euro_price, ',', '.'), 0),
                        rg = NULLIF(UPPER(TRIM(@rg)), ''),
                        warning = NULLIF(TRIM(@warning), ''),
                        cent_pfand = NULLIF(100 * @euro_pfand, 0),
                        weight = NULLIF(1 * @weight, 0),
                        teileart = NULLIF(1 * TRIM(TRIM(BOTH '\r' FROM @teileart)), 0)
                    ;
SQL
                );
            }
            //
            $pdo->exec(<<<SQL
                DELETE FROM raw_part_rows
                WHERE
                    part_number IS NULL
                AND
                    catalog_import_id = {$this->import->id}
                ;
SQL
            );

            $this->import->step = CatalogImport::STEP_RAW_READ;
            $this->import->raw_read_time_taken = microtime(true) - $startedAt;
            $this->import->raw_read_memory_peak = memory_get_peak_usage() / 1024 / 1024;
            $this->import->raw_read_lines =
                DB::table('raw_part_rows')
                    ->where('catalog_import_id', $this->import->id)
                    ->count();
            //
            $this->import->save();
        }
    }
}
