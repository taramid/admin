<?php

namespace App\Jobs\CatalogImporting;

use App\CatalogImport;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class Apply implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    protected $import;

    protected $pdo;

    protected $now;

    protected $nRgCreated;
    protected $nParts;

    protected $rgs;

    /**
     * Create a new job instance.
     *
     * @param  \App\CatalogImport  $catalogImport
     * @return void
     */
    public function __construct(CatalogImport $catalogImport)
    {
        $this->import = $catalogImport;
    }

    /**
     * Set up (prepare) for handling
     */
    protected function __init()
    {
        $this->pdo = DB::connection()->getPdo();

        $this->now = Carbon::now();

        $this->nRgCreated = 0;
        $this->nParts = 0;

        $this->rgs = new class($this->import->supply_id, $this->now)
        {
            private $supplyId;
            private $now;
            private $rgs;

            public function __construct($supplyId, $now)
            {
                $this->supplyId = $supplyId;
                $this->now = $now;

                $this->rgs = DB::table('rgs')
                    ->select(['id', 'name'])
                    ->where('supply_id', $supplyId)
                    ->get()
                    ->pluck('id', 'name')
                    ->toArray();
            }

            public function getId($rg)
            {
                return
                    $this->rgs[$rg] ?? null;
            }

            public function create($rg)
            {
                $id = DB::table('rgs')
                    ->insertGetId([
                        'supply_id' => $this->supplyId,
                        'name' => $rg,
                        'created_at' => $this->now
                    ]);

                if ($id) {
                    $this->rgs[$rg] = $id;
                }

                return $id;
            }
        };
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $startedAt = microtime(true);

        if (CatalogImport::STEP_APPROVED === $this->import->step) {

            $this->__init();

            DB::transaction(function () {

                $this->pdo->exec('SET @now = \'' . $this->now . '\';');

                $this->softDeleteExistingParts();

                DB::table('raw_part_rows')
                    ->distinct()
                    ->select('rg')
                    ->where('catalog_import_id', $this->import->id)
                    ->whereNotNull('rg')
                    ->get()
                    ->pluck('rg')
                    ->each(function ($rg) {

                        $rgId = $this->rgs->getId($rg);

                        DB::table('raw_part_rows')
                            ->where('catalog_import_id', $this->import->id)
                            ->where('rg', $rg)
                            ->whereRaw('CHAR_LENGTH(part_number) >= ?', [config('catalog_import.part_number_min_length')])
                            ->whereNotNull('cent_price')
                            ->whereNotNull('rg')
                            ->chunkById(config('catalog_import.chunk_size'), function ($rows) use ($rg, &$rgId) {

                                $parts = [];

                                $rows->each(function ($raw) use (&$parts) {
                                    //
                                    array_push($parts, [
                                        'part_number' => $raw->part_number,
                                        'cent_price' => $raw->cent_price,
                                        'warning' => $raw->warning,
                                        'cent_pfand' => $raw->cent_pfand,
                                        'weight' => $raw->weight,
                                        'teileart' => $raw->teileart,
                                    ]);
                                });

                                if (!empty($parts)) {
                                    //
                                    if (!isset($rgId)) {
                                        $rgId = $this->rgs->create($rg);
                                        ++$this->nRgCreated;
                                    }
                                    $this->insertParts($rgId, $parts);
                                    $this->nParts += count($parts);
                                }

                                $this->markProcessedRaw($rows->pluck('id')->toArray());
                            });

                    });

                $this->deleteProcessedRaw();
            });

            $this->import->step = CatalogImport::STEP_IMPORTED;
            //
            $this->import->import_time_taken = microtime(true) - $startedAt;
            $this->import->import_memory_peak = memory_get_peak_usage() / 1024 / 1024;
            $this->import->import_rg_created = $this->nRgCreated;
            $this->import->import_parts = $this->nParts;
            //
            $this->import->save();

            //
            $this->import->supply->is_in_maintenance = false;
            $this->import->supply->save();
        }
    }

    /**
     * @return int amount deleted
     */
    private function softDeleteExistingParts()
    {
        return
            DB::table('supplies')
                ->leftJoin('rgs', 'rgs.supply_id', '=', 'supplies.id')
                ->leftJoin('parts', 'parts.rg_id', '=', 'rgs.id')
                ->where('supplies.id', '=', $this->import->supply_id)
                ->whereNull('parts.deleted_at')
                ->update(['parts.deleted_at' => $this->now]);
    }

    /**
     * Do the job  - insert new parts or update existing
     *
     * note: ON DUPLICATE KEY UPDATE approach increases AUTO_INCREMENT on the catalog size each time
     *
     * @param int $rgId RG id
     * @param array $parts parts values
     */
    private function insertParts($rgId, $parts)
    {
        if (isset($rgId) && !empty($parts)) {

            $questionMarks = implode(
                ',',
                array_map(
                    function () {
                        return '(?,?,@rg,?,?,?,?,@now)';
                    },
                    $parts
                )
            );

            $query = <<<SQL
                INSERT INTO parts
                (part_number, cent_price, rg_id, warning, cent_pfand, weight, teileart, created_at)
                VALUES
                {$questionMarks}
                ON DUPLICATE KEY UPDATE
                    cent_price = VALUES(cent_price),
                    warning = VALUES(warning),
                    cent_pfand = VALUES(cent_pfand),
                    weight = VALUES(weight),
                    teileart = VALUES(teileart),
                    updated_at = @now,
                    deleted_at = NULL;
SQL;

            $this->pdo->exec('SET @rg = ' . (int)$rgId . ';');

            $statement = $this->pdo->prepare($query);

            $values = [];
            foreach ($parts as $p) {
                array_push(
                    $values,
                    $p['part_number'],
                    $p['cent_price'],
                    $p['warning'],
                    $p['cent_pfand'],
                    $p['weight'],
                    $p['teileart']
                );
            }
            unset($p);

            $statement->execute($values);
        }
    }

    /**
     * Mark processed raw records for further removal
     *
     * @param array $processedRawIds processed raw records id
     * @return int amount marked
     */
    public function markProcessedRaw($processedRawIds)
    {
        return
            DB::table('raw_part_rows')
                ->whereIn('id', $processedRawIds)
                ->update(['imported' => true]);
    }

    /**
     * Delete previously marked processed raw records
     *
     * @return int amount deleted
     */
    public function deleteProcessedRaw()
    {
        return
            DB::table('raw_part_rows')
                ->where('imported', true)
                ->delete();
    }
}
