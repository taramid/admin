<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Upload
 * @package App
 *
 * @property int $id
 * @property int $admin_id
 * @property int $human_id
 * @property \Autocarat\Core\Admin $admin
 * @property array $files
 */
class Upload extends Model
{
    protected $fillable = [
        'admin_id',
        'human_id'
    ];

    public function admin()
    {
        return $this->belongsTo('Autocarat\Core\Admin');
    }

    public function files()
    {
        return $this->hasMany('App\UploadedFile');
    }
}
