<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        \Autocarat\Core\Admin::class => \App\Policies\AdminPolicy::class,
        \Autocarat\Core\Manufacturer::class => \App\Policies\ManufacturerPolicy::class,
        \Autocarat\Core\Supplier::class => \App\Policies\SupplierPolicy::class,
        \Autocarat\Core\Supply::class => \App\Policies\SupplyPolicy::class,
        \Autocarat\Core\Human::class => \App\Policies\HumanPolicy::class,
        \Autocarat\Core\Imp::class => \App\Policies\ImpPolicy::class,
        \Autocarat\Core\Minion::class => \App\Policies\MinionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Gate::define('manage-catalogs', 'App\Policies\SupplyPolicy@view');
    }
}
