<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RawPartRow
 * @package App
 *
 * @property int $id
 * @property int $catalog_import_id
 * @property \App\CatalogImport $catalogImport
 */
class RawPartRow extends Model
{
    public $timestamps = false;

    public function catalogImport()
    {
        return $this->belongsTo('App\CatalogImport');
    }
}
