<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CatalogImport
 * @package App
 *
 * @property int $id
 * @property int $supply_id
 * @property int $upload_id
 * @property int $step
 * @property float $raw_read_time_taken
 * @property float $raw_read_memory_peak
 * @property int $raw_read_lines
 * @property float $import_time_taken
 * @property float $import_memory_peak
 * @property int $import_rg_created
 * @property int $import_parts
 * @property \App\Supply $supply
 * @property \App\Upload $upload
 * @property \Illuminate\Database\Eloquent\Collection $files
 */
class CatalogImport extends Model
{
    const STEP_UPLOADED = 20;
    const STEP_RAW_READ = 22;
    const STEP_APPROVED = 30;
    const STEP_IMPORTED = 33;

    protected $fillable = [
        'supply_id', 'upload_id', 'step',
    ];

    public function supply()
    {
        return $this->belongsTo('App\Supply');
    }

    public function upload()
    {
        return $this->belongsTo('App\Upload');
    }

    public function rows()
    {
        return $this->hasMany('App\RawPartRow');
    }

    public function files()
    {
        return $this->upload->files();
    }
}
