<?php

namespace App;

use Autocarat\Core\Supply as CoreSupply;
use Illuminate\Support\Facades\DB;

/**
 * Class Upload
 * @package App
 *
 * @property \Illuminate\Database\Eloquent\Collection $catalogImports
 * @property \Illuminate\Database\Eloquent\Collection $rgsSorted
 * @property \Illuminate\Support\Collection $discounts
 */
class Supply extends CoreSupply
{
    public function catalogImports()
    {
        return $this->hasMany('App\CatalogImport')->orderBy('created_at', 'desc');
    }

    /**
     * @return array
     */
    public function discountMatrix()
    {
        $saleDiscounts = DB::table('discounts')
            ->select([
                'discounts.id',
                'discounts.rg_id',
                'discounts.loyalty_id',
                'discounts.percent',
            ])
            ->leftJoin('rgs', 'discounts.rg_id', '=', 'rgs.id')
            ->where([
                'rgs.supply_id' => $this->id,
            ])
            ->get()
            ->groupBy('rg_id')
            ->map(function ($rg) {
                return $rg->map(function ($sale) {
                    return json_decode(json_encode($sale), true);
                })->groupBy('loyalty_id');
            })
            ->toArray();

        $matrix = $this->rgs->map(function ($rg) {
            unset($rg->created_at);
            unset($rg->updated_at);
            return $rg;
        })->toArray();

        foreach ($matrix as &$rg) {
            $rg['sales'] = [];
            foreach ($this->loyalties as $loyalty) {
                $rg['sales'][] = $saleDiscounts[$rg['id'] ?? null][$loyalty->id ?? null][0] ?? [];
            }
        }
        unset($rg);

        usort($matrix, ['self', 'sortRg']);

        return $matrix;
    }

    /**
     * proper RG name comparison (for sorting)
     *
     * @param array|\stdClass $a
     * @param array|\stdClass $b
     * @return int
     */
    public static function sortRg($a, $b)
    {
        $aName = $a['name'] ?? $a->name;
        $bName = $b['name'] ?? $b->name;
        if (is_numeric($aName) && is_numeric($bName)) {
            return intval($aName) > intval($bName) ? 1 : -1;
        }
        return $aName > $bName ? 1 : -1;
    }
}
