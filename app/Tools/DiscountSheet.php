<?php

namespace App\Tools;

use App\Supply;
use \Illuminate\Http\UploadedFile;

class DiscountSheet
{
    const XLSX_RG_COLUMN = 1;
    const XLSX_PURCHASE_COLUMN = 1 + self::XLSX_RG_COLUMN;
    const XLSX_DATA_COLUMN = 1 + self::XLSX_PURCHASE_COLUMN;
    //
    const XLSX_HEADER_ROW = 1;
    const XLSX_LOYALTY_HEADER_ROW = 1 + self::XLSX_HEADER_ROW;
    const XLSX_DATA_ROW = 1 + self::XLSX_LOYALTY_HEADER_ROW;

    private static $sheet = null;
    private static $_THE_LAST_COLUMN = null;
    private static $_THE_LAST_ROW = null;

    private static $bHasNoChanges = true;

    /**
     * @param Supply $supply
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public static function createSheet(Supply $supply)
    {
        $matrix = $supply->discountMatrix();

        //
        self::$_THE_LAST_COLUMN = self::XLSX_DATA_COLUMN + max(0, $supply->loyalties->count() - 1);
        self::$_THE_LAST_ROW = self::XLSX_DATA_ROW + max(0, count($matrix) - 1);

        //
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        self::$sheet = $spreadsheet->getActiveSheet();

        self::$sheet->mergeCellsByColumnAndRow(
            self::XLSX_RG_COLUMN,
            self::XLSX_HEADER_ROW,
            self::XLSX_RG_COLUMN,
            self::XLSX_LOYALTY_HEADER_ROW
        );
        self::$sheet->mergeCellsByColumnAndRow(
            self::XLSX_PURCHASE_COLUMN,
            self::XLSX_HEADER_ROW,
            self::XLSX_PURCHASE_COLUMN,
            self::XLSX_LOYALTY_HEADER_ROW
        );
        self::$sheet->mergeCellsByColumnAndRow(
            self::XLSX_DATA_COLUMN,
            self::XLSX_HEADER_ROW,
            self::$_THE_LAST_COLUMN,
            self::XLSX_HEADER_ROW
        );

        self::$sheet->setCellValueExplicitByColumnAndRow(
            self::XLSX_RG_COLUMN,
            self::XLSX_HEADER_ROW,
            __('terms.rg'),
            \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
        );
        self::$sheet->setCellValueExplicitByColumnAndRow(
            self::XLSX_PURCHASE_COLUMN,
            self::XLSX_HEADER_ROW,
            __('discounts.purchase'),
            \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
        );
        self::$sheet->setCellValueExplicitByColumnAndRow(
            self::XLSX_DATA_COLUMN,
            self::XLSX_HEADER_ROW,
            __('discounts.sale'),
            \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
        );

        foreach ($supply->loyalties as $l => $loyalty) {
            self::$sheet->setCellValueExplicitByColumnAndRow(
                self::XLSX_DATA_COLUMN + $l,
                self::XLSX_LOYALTY_HEADER_ROW,
                $loyalty->name,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
            );
        }

        $r = 0;
        foreach ($matrix as $rg) {
            self::$sheet->setCellValueExplicitByColumnAndRow(
                self::XLSX_RG_COLUMN,
                self::XLSX_DATA_ROW + $r,
                $rg['name'],
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
            );
            //
            if (isset($rg['percent'])) {
                self::$sheet->setCellValueExplicitByColumnAndRow(
                    self::XLSX_PURCHASE_COLUMN,
                    self::XLSX_DATA_ROW + $r,
                    $rg['percent'],
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC
                );
            }
            //
            foreach ($rg['sales'] as $l => $sale) {
                if (isset($sale['percent'])) {
                    self::$sheet->setCellValueExplicitByColumnAndRow(
                        self::XLSX_DATA_COLUMN + $l,
                        self::XLSX_DATA_ROW + $r,
                        $sale['percent'],
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC
                    );
                }
            }
            //
            ++$r;
        }

        self::$sheet->setTitle("$supply->id $supply->name");

        self::style();

        return response()->streamDownload(
            function () use ($spreadsheet) {
                $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
                $writer->save('php://output');
            },
            __('discounts.filename', [
                'id' => $supply->id,
                'supply' => $supply->name
            ]) . '.xlsx',
            [
                'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ]
        );
    }


    private static function style()
    {
        self::$sheet->getStyleByColumnAndRow(
            self::XLSX_RG_COLUMN,
            self::XLSX_HEADER_ROW,
            self::XLSX_DATA_COLUMN,
            self::XLSX_HEADER_ROW
        )->getFont()->getColor()->setARGB('ff808080');
        //
        self::$sheet->getStyleByColumnAndRow(
            self::XLSX_DATA_COLUMN,
            self::XLSX_LOYALTY_HEADER_ROW,
            self::$_THE_LAST_COLUMN,
            self::XLSX_LOYALTY_HEADER_ROW
        )->getFont()->getColor()->setARGB('ffffa500');
        //
        self::$sheet->getStyleByColumnAndRow(
            self::XLSX_RG_COLUMN,
            self::XLSX_DATA_ROW,
            self::XLSX_RG_COLUMN,
            self::$_THE_LAST_ROW
        )->getFont()->getColor()->setARGB('ff808080');
        //
        self::$sheet->getStyleByColumnAndRow(
            self::XLSX_PURCHASE_COLUMN,
            self::XLSX_DATA_ROW,
            self::XLSX_PURCHASE_COLUMN,
            self::$_THE_LAST_ROW
        )->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
        //
        self::$sheet->getStyleByColumnAndRow(
            self::XLSX_RG_COLUMN,
            self::XLSX_HEADER_ROW,
            self::$_THE_LAST_COLUMN,
            self::XLSX_LOYALTY_HEADER_ROW
        )->getFont()->setBold(true);
        //
        self::$sheet->getStyleByColumnAndRow(
            self::XLSX_RG_COLUMN,
            self::XLSX_HEADER_ROW,
            self::XLSX_RG_COLUMN,
            self::$_THE_LAST_ROW
        )->getFont()->setBold(true);
        //
        $whole = self::$sheet->getStyleByColumnAndRow(
            self::XLSX_RG_COLUMN,
            self::XLSX_HEADER_ROW,
            self::$_THE_LAST_COLUMN,
            self::$_THE_LAST_ROW
        );
        $whole->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $whole->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        self::$sheet->getColumnDimensionByColumn(self::XLSX_PURCHASE_COLUMN)->setWidth(11);
    }


    public static function parse(Supply $supply, UploadedFile $file)
    {
        self::$bHasNoChanges = true;

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($file->path());

        self::$sheet = $spreadsheet->getActiveSheet();

        if ($supply->id === self::getSupplyIdFromSheetTitle()) {

            $loyalties = self::prepareLoyaltyMods($supply);
            $percentages = self::preparePercentageMods($supply, $loyalties);

            return [
                'loyalties' => $loyalties,
                'percentages' => $percentages,
                'hasNoChanges' => self::$bHasNoChanges
            ];
        }

        return null;
    }


    private static function prepareLoyaltyMods($supply)
    {
        $modified = $supply->loyalties->map(function ($lo) {
            unset($lo->created_at);
            unset($lo->updated_at);
            return $lo;
        })->toArray();

        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString(self::$sheet->getHighestDataColumn());
        for ($column = self::XLSX_DATA_COLUMN; $column <= $highestColumnIndex; ++$column) {
            $renameTo = null;
            $cell = self::$sheet->getCellByColumnAndRow($column, self::XLSX_LOYALTY_HEADER_ROW, false);
            if (isset($cell)) {
                $renameTo = trim($cell->getValue());
                if (0 === mb_strlen($renameTo)) {
                    $renameTo = null;
                }
            }
            /*
             * old == new       null
             * old != new       id,new      (mod)
             * old     -        null
             *  -     new       -,new       (create)
             *  -      -        -,-         (create)
             */
            $nth = $column - self::XLSX_DATA_ROW;
            if (isset($modified[$nth])) {
                if (isset($renameTo) && $renameTo !== ($modified[$nth]['name'] ?? null)) {
                    $modified[$nth]['renameTo'] = $renameTo;
                    self::$bHasNoChanges = false;
                }
            } else {
                $modified[] = [
                    'supply_id' => $supply->id,
                    'renameTo' => $renameTo
                ];
                self::$bHasNoChanges = false;
            }
        }

        return $modified;
    }


    private static function getSupplyIdFromSheetTitle()
    {
        return
            intval(
                explode(
                    ' ',
                    (isset(self::$sheet) ? self::$sheet->getTitle() : null)
                )[0]
            ) ?? null;
    }


    private static function preparePercentageMods($supply, $loyalties)
    {
        $matrix = $supply->discountMatrix();

        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString(self::$sheet->getHighestDataColumn());
        $highestRowIndex = self::$sheet->getHighestDataRow();

        for ($row = self::XLSX_DATA_ROW; $row <= $highestRowIndex; ++$row) {
            $rgCell = self::$sheet->getCellByColumnAndRow(self::XLSX_RG_COLUMN, $row, false);
            if (isset($rgCell)) {
                $rgName = mb_strtoupper(trim($rgCell->getValue()));
                if (0 < mb_strlen($rgName)) {

                    $matrixRgIndex = -1;
                    for ($r = 0; $r < count($matrix); ++$r) {
                        if (($matrix[$r]['name'] ?? null) === $rgName) {
                            $matrixRgIndex = $r;
                            break;
                        }
                    }
                    if (-1 === $matrixRgIndex) {
                        $matrix[] = [
                            'supply_id' => $supply->id,
                            'name' => $rgName,
                            'sales' => []
                        ];
                        $matrixRgIndex = count($matrix) - 1;
                        self::$bHasNoChanges = false;
                    }
                    $matrixRg = &$matrix[$matrixRgIndex];


                    // purchase %

                    $cell = self::$sheet->getCellByColumnAndRow(self::XLSX_PURCHASE_COLUMN, $row, false);
                    if (isset($cell)) {
                        $val = $cell->getValue();
                        $percent = is_numeric($val) ? floatval($val) : null;
                        if (isset($percent) && floatval($matrixRg['percent'] ?? null) !== $percent) {
                            $matrixRg['percentTo'] = $percent;
                            self::$bHasNoChanges = false;
                        }
                    }

                    // sale %%

                    $matrixRg['sales'] = array_merge(
                        $matrixRg['sales'],
                        array_fill(
                            0,
                            count($loyalties) - count($matrixRg['sales']),
                            []
                        )
                    );

                    for ($column = self::XLSX_DATA_COLUMN; $column <= $highestColumnIndex; ++$column) {

                        $nth = $column - self::XLSX_DATA_ROW;

                        $sale = &$matrixRg['sales'][$nth] ?? null;

                        $percent = null;
                        $cell = self::$sheet->getCellByColumnAndRow($column, $row, false);
                        if (isset($cell)) {
                            $val = $cell->getValue();
                            $percent = is_numeric($val) ? floatval($val) : null;
                        }

                        if (isset($percent)) {
                            if (empty($sale)) {
                                $sale = [
                                    'rg_id' => $matrixRg['id'] ?? null,
                                    'loyalty_id' => $loyalties[$nth]['id'] ?? null,
                                    'percentTo' => $percent
                                ];
                                self::$bHasNoChanges = false;
                            } else {
                                if (floatval($sale['percent'] ?? null) !== $percent) {
                                    $sale['percentTo'] = $percent;
                                    self::$bHasNoChanges = false;
                                }
                            }
                        }

                        unset($sale);
                    }

                    unset($matrixRg);
                }
            }
        }

        usort(
            $matrix,
            ['App\Supply', 'sortRg']
        );

        return $matrix;
    }
}
