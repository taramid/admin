<?php

namespace App\Http\Controllers;

use Autocarat\Core\Human;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class HumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Human::class);

        return view('humans.index', [
            'humans' => Human::orderBy('id')->with(['imps:id,code'])->paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Human::class);

        return view('humans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Human::class);

        $this->validate($request, [
            'name' => 'required|min:2',
            'email' => 'required|email|unique:humans',
            'note' => 'nullable'
        ]);

        $human = new Human();
        $human->name = $request->input('name');
        $human->email = $request->input('email');
        $human->note = $request->input('note');

        $human->save();

        return redirect()->route('humans.index')
            ->with(
                'success',
                __('humans.create_success', ['name' => $request->input('name')])
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Autocarat\Core\Human  $human
     * @return \Illuminate\Http\Response
     */
    public function show(Human $human)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Autocarat\Core\Human  $human
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Human $human)
    {
        $this->authorize('create', $human);

        return view('humans.edit', compact('human'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Autocarat\Core\Human  $human
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Human $human)
    {
        $this->authorize('create', $human);

        $this->validate($request, [
            'name' => 'required|min:2',
            'email' => [
                'required',
                'email',
                Rule::unique('humans')->ignore($human->id)  // https://laravel.com/docs/5.5/validation#rule-unique
            ],
            'note' => 'nullable'
        ]);

        $name = $human->name;

        $human->name = $request->input('name');
        $human->email = $request->input('email');
        $human->note = $request->input('note');

        $human->save();

        return redirect()->route('humans.index')
            ->with(
                'success',
                __('humans.update_success', ['name' => $name])
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Autocarat\Core\Human  $human
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Human $human)
    {
        $this->authorize('delete', $human);
    }
}
