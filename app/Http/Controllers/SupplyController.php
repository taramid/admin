<?php

namespace App\Http\Controllers;

use Autocarat\Core\Manufacturer;
use Autocarat\Core\Supplier;
use App\Supply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Supply::class);

        $supplies = null;

        $user = Auth::user();

        if ($user->isAdmin()) {
            $supplies = Supply::orderBy('name')->with(['manufacturer:id,name', 'supplier:id,name'])->get();
        } else {
            $supplies = $user->supplies()->with(['manufacturer:id,name', 'supplier:id,name'])->get();
        }

        return view('supplies.index', [
            'supplies' => $supplies
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Supply::class);

        return view('supplies.create', [
            'manufacturers' => Manufacturer::orderBy('name')->get(['id as value', 'name as title']),
            'suppliers' => Supplier::orderBy('name')->get(['id as value', 'name as title'])
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Supply::class);

        $this->validate($request, [
            'name' => 'required|min:2',
            'manufacturer' => 'required|in:' . Manufacturer::get(['id'])->implode('id', ','),
            'supplier' => 'required|in:' . Supplier::get(['id'])->implode('id', ','),
            'note' => 'nullable'
        ]);

        $supply = new Supply();
        $supply->name = $request->input('name');
        $supply->manufacturer_id = $request->input('manufacturer');
        $supply->supplier_id = $request->input('supplier');
        $supply->alias = $request->input('name');
        $supply->note = $request->input('note');

        $supply->save();

        return redirect()->route('supplies.index')
            ->with(
                'success',
                __('supplies.create_success', ['name' => $request->input('name')])
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Supply $supply)
    {
        $this->authorize('view', $supply);

        return view('supplies.show', compact('supply'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Supply $supply)
    {
        $this->authorize('update', $supply);

        return view('supplies.edit', [
            'supply' => $supply,
            'manufacturers' => Manufacturer::orderBy('name')->get(['id as value', 'name as title']),
            'suppliers' => Supplier::orderBy('name')->get(['id as value', 'name as title'])
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Supply $supply)
    {
        $this->authorize('update', $supply);

        $this->validate($request, [
            'name' => 'required|min:2',
            'manufacturer' => 'required|in:' . Manufacturer::get(['id'])->implode('id', ','),
            'supplier' => 'required|in:' . Supplier::get(['id'])->implode('id', ','),
            'note' => 'nullable'
        ]);

        $name = $supply->name;

        $supply->name = $request->input('name');
        $supply->manufacturer_id = $request->input('manufacturer');
        $supply->supplier_id = $request->input('supplier');
        $supply->alias = $request->input('name');
        $supply->note = $request->input('note');

        $supply->save();

        return redirect()->route('supplies.index')
            ->with(
                'success',
                __('supplies.update_success', ['name' => $name])
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Supply $supply)
    {
        $this->authorize('delete', $supply);

        // todo: check if there no order items for the supply

//        if ($supply->[order-items]->isEmpty()) {
        $name = $supply->name;
        $supply->delete();
        return redirect()->route('supplies.index')
            ->with(
                'success',
                __('supplies.delete_success', ['name' => $name])
            );
//        }
//        else {
//            return redirect()->back()
//                ->withErrors(
//                    __(
//                        'supplies.cant_delete_...',
//                        ['name' => $supply->name]
//                    )
//                );
//        }
    }
}
