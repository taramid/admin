<?php

namespace App\Http\Controllers;

use App\Supply;
use App\Tools\DiscountSheet;
use Autocarat\Core\Discount;
use Autocarat\Core\Loyalty;
use Autocarat\Core\RG;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Supply $supply)
    {
        $this->authorize('view', $supply);

        $matrix = $supply->discountMatrix();

        return view(
            'discounts.index',
            compact('supply', 'matrix')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Autocarat\Core\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function show(Discount $discount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Autocarat\Core\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount $discount)
    {
        //
    }

    /**
     * @param Supply $supply
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function export(Supply $supply)
    {
        return DiscountSheet::createSheet($supply);
    }

    /**
     * @param Request $request
     * @param Supply $supply
     * @return \Illuminate\Http\RedirectResponse
     */
    public function upload(Request $request, Supply $supply)
    {
        $file = $request->file('xlsx');
        if (isset($file)) {
            //
            $mods = DiscountSheet::parse($supply, $file);
            if (isset($mods)) {
                if ($mods['hasNoChanges']) {
                    return redirect()
                        ->route('discounts.index', ['supply' => $supply->id])
                        ->withErrors(
                            __('discounts.upload_has_no_changes')
                        );
                } else {
                    return redirect()
                        ->route('discounts.preview', ['supply' => $supply->id])
                        ->with('loyalties', $mods['loyalties'])
                        ->with('percentages', $mods['percentages']);
                }
            } else {
                return redirect()
                    ->route('discounts.index', ['supply' => $supply->id])
                    ->withErrors(
                        __('discounts.upload_note_wrong_supply_id')
                    );
            }
        } else {
            return redirect()
                ->back()
                ->withErrors(
                    __('general.upload_empty')
                );
        }
    }

    /**
     * @param Supply $supply
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function preview(Supply $supply)
    {
        if (session()->has('loyalties') && session()->has('percentages')) {
            session()->keep(['loyalties', 'percentages']);
            return view(
                    'discounts.preview',
                    [
                        'supply' => $supply,
                        'loyalties' => session('loyalties'),
                        'percentages' => session('percentages')
                    ]
                );
        } else {
            return redirect()
                ->route('discounts.index', ['supply' => $supply->id]);
        }
    }

    /**
     * @param Supply $supply
     * @return \Illuminate\Http\RedirectResponse
     */
    public function apply(Supply $supply)
    {
        if (session()->has('loyalties') && session()->has('percentages')) {

            DB::transaction(function () use ($supply) {

                foreach (session('loyalties') as $lo) {
                    if (isset($lo['id'])) {
                        if (isset($lo['renameTo'])) {
                            $loyalty = Loyalty::find($lo['id']);
                            $loyalty->name = $lo['renameTo'];
                            $loyalty->save();
                        }
                    } else {
                        $loyalty = new Loyalty;
                        $loyalty->supply_id = $supply->id;
                        $loyalty->name = $lo['renameTo'] ?? null;
                        $loyalty->save();
                    }
                }
                $loyalties = $supply->loyalties;

                foreach (session('percentages') as $r) {

                    if (!isset($r['id'])) {
                        $rg = new RG;
                        $rg->supply_id = $supply->id;
                        $rg->name = $r['name'];
                        $rg->save();

                        $r['id'] = $rg->id;
                    }

                    // purchase
                    if (isset($r['percentTo'])) {
                        $rg = RG::find($r['id']);
                        $rg->percent = $r['percentTo'];
                        $rg->save();
                    }

                    // sales
                    foreach ($r['sales'] as $l => $sale) {
                        if (isset($sale['percentTo'])) {
                            if (isset($sale['id'])) {
                                $discount = Discount::find($sale['id']);
                                $discount->percent = $sale['percentTo'];
                                $discount->save();
                            } else {
                                $discount = new Discount;
                                $discount->rg_id = $r['id'];
                                $discount->loyalty_id = $loyalties[$l]->id;
                                $discount->percent = $sale['percentTo'];
                                $discount->save();
                            }
                        }
                    }
                }
            });

            return redirect()
                ->route('discounts.index', ['supply' => $supply->id])
                ->with('success', __('discounts.apply_success'));
        } else {
            return redirect()
                ->route('discounts.index', ['supply' => $supply->id])
                ->withErrors('no data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Autocarat\Core\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function destroy(Discount $discount)
    {
        //
    }
}
