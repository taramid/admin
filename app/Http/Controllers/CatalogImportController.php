<?php

namespace App\Http\Controllers;

use App\CatalogImport;
use App\Jobs\CatalogImporting;
use App\Supply;
use App\Upload;
use App\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class CatalogImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function index(Supply $supply)
    {
        if (Gate::denies('manage-catalogs', $supply)) {
            abort(403);
        }

        return view('catalog_imports.index', [
            'supply' => $supply,
            'catalog_imports' => $supply->catalogImports()->withCount('rows')->get()
        ]);
    }

    /**
     * Show the form for uploading catalog.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function create(Supply $supply)
    {
        if (Gate::denies('manage-catalogs', $supply)) {
            abort(403);
        }

        return view('catalog_imports.create', ['supply' => $supply]);
    }

    /**
     * Upload catalog.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request, Supply $supply)
    {
        if (Gate::denies('manage-catalogs', $supply)) {
            abort(403);
        }

        $files = $request->file('csv');
        if (isset($files)) {
            //
            $upload = new Upload();
            $upload->admin_id = Auth::id();
            $upload->save();
            //
            foreach ($files as $file) {
                //
                $uploadedFile = new UploadedFile();
                $uploadedFile->upload_id = $upload->id;
                $uploadedFile->name = $file->getClientOriginalName();
                $uploadedFile->path = $file->store(env('PATH_CATALOGS', 'catalogs'));
                $uploadedFile->save();
            }
            //
            $import = new CatalogImport();
            $import->supply_id = $supply->id;
            $import->upload_id = $upload->id;
            $import->step = CatalogImport::STEP_UPLOADED;
            $import->save();
            //
            CatalogImporting\RawRead::dispatch($import);

            return redirect()->route('catalog_imports.index', [
                'supply' => $supply->id,
                'catalog_imports' => $supply->catalogImports
            ])
                ->with(
                    'success',
                    trans_choice('catalog_imports.upload_success', count($files), [
                        'id' => $import->id,
                        'files' => implode(
                            '<br>',
                            array_map(
                                function ($file) {
                                    return $file->getClientOriginalName();
                                },
                                $files
                            )
                        )
                    ])
                )
                ->with(
                    'note',
                    trans_choice('catalog_imports.upload_note', count($files))
                );
        }
        else {
            return redirect()->back()
                ->withErrors(
                    __('general.upload_empty')
                );
        }

    }

    /**
     * returns multiple random rows as JSON
     *
     * @param  \App\Supply  $supply
     * @param  \App\CatalogImport  $import
     * @return \Illuminate\Http\Response
     */
    public function preview(Supply $supply, CatalogImport $import)
    {
        if ($supply->id === $import->supply_id) {
            if (Gate::allows('manage-catalogs', $supply)) {
                //
                return response()->json([
                    'rows' => DB::table('raw_part_rows')
                        ->select('part_number', 'cent_price', 'rg', 'warning', 'cent_pfand', 'weight', 'teileart')
                        ->where('catalog_import_id', $import->id)
                        ->inRandomOrder()
                        ->limit(5)
                        ->get(),
                    'files' => $import->files
                ]);
            }
        }
        return response()->json([]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Supply  $supply
     * @param  \App\CatalogImport  $import
     * @return \Illuminate\Http\Response
     */
    public function apply(Supply $supply, CatalogImport $import)
    {
// todo: pass only CatalogImport, and checking Gate get supply from CI instance 
        if (Gate::denies('manage-catalogs', $supply) || $import->supply_id !== $supply->id) {
            abort(403);
        }

        if (CatalogImport::STEP_RAW_READ === $import->step) {

            $supply->is_in_maintenance = true;
            $supply->save();

            $import->step = CatalogImport::STEP_APPROVED;
            $import->save();

            CatalogImporting\Apply::dispatch($import);

            return redirect()->route('catalog_imports.index', [
                'supply' => $supply->id,
                'catalog_imports' => $supply->catalogImports
            ])
                ->with(
                    'success',
                    __('catalog_imports.apply_success', ['id' => $import->id])
                );
        }
        else {
            return redirect()->back()
                ->withErrors(__('catalog_imports.cant_apply', ['id' => $import->id]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supply  $supply
     * @param  \App\CatalogImport  $import
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Supply $supply, CatalogImport $import)
    {
        if (Gate::denies('manage-catalogs', $supply) || $import->supply_id !== $supply->id) {
            abort(403);
        }

        if (CatalogImport::STEP_RAW_READ === $import->step || CatalogImport::STEP_IMPORTED === $import->step) {

            $id = $import->id;
            $import->delete();

            return redirect()->route('catalog_imports.index', [
                'supply' => $supply->id,
                'catalog_imports' => $supply->catalogImports
            ])
                ->with(
                    'success',
                    __('catalog_imports.delete_success', ['id' => $id])
                );
        } else {
            return redirect()->back()
                ->withErrors(__('catalog_imports.cant_delete_now', ['id' => $import->id]));
        }
    }
}
