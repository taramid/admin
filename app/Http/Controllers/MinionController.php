<?php

namespace App\Http\Controllers;

use Autocarat\Core\Minion;
use Illuminate\Http\Request;

class MinionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Minion::class);

        return view('minions.index', [
            'minions' => Minion::orderBy('name')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Minion::class);

        return view('minions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Minion::class);

        $this->validate($request, [
            'name' => 'required|min:2',
            'code' => 'required|min:2',
            'note' => 'nullable'
        ]);

        $minion = new Minion();
        $minion->name = $request->input('name');
        $minion->code = $request->input('code');
        $minion->note = $request->input('note');

        $minion->save();

        return redirect()->route('minions.index')
            ->with(
                'success',
                __('minions.create_success', ['name' => $request->input('name')])
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Autocarat\Core\Minion  $minion
     * @return \Illuminate\Http\Response
     */
    public function show(Minion $minion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Autocarat\Core\Minion  $minion
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Minion $minion)
    {
        $this->authorize('update', $minion);

        return view('minions.edit', compact('minion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Autocarat\Core\Minion  $minion
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Minion $minion)
    {
        $this->authorize('update', $minion);

        $this->validate($request, [
            'name' => 'required|min:2',
            'code' => 'required|min:2',
            'note' => 'nullable'
        ]);

        $name = $minion->name;

        $minion->name = $request->input('name');
        $minion->code = $request->input('code');
        $minion->note = $request->input('note');

        $minion->save();

        return redirect()->route('minions.index')
            ->with(
                'success',
                __('minions.update_success', ['name' => $name])
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Autocarat\Core\Minion  $minion
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Minion $minion)
    {
        $this->authorize('delete', $minion);

        $name = $minion->name;
        $minion->delete();
        return redirect()->route('minions.index')
            ->with(
                'success',
                __('minions.delete_success', ['name' => $name])
            );
    }
}
