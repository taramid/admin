<?php

namespace App\Http\Controllers;

use Autocarat\Core\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Supplier::class);

        $suppliers = Supplier::orderBy('name')->get();

        return view('suppliers.index', [
            'suppliers' => $suppliers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Supplier::class);

        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Supplier::class);

        $this->validate($request, [
            'name' => 'required|min:2',
            'note' => 'nullable'
        ]);

        $supplier = new Supplier();
        $supplier->name = $request->input('name');
        $supplier->note = $request->input('note');

        $supplier->save();

        return redirect()->route('suppliers.index')
            ->with(
                'success',
                __('suppliers.create_success', ['name' => $request->input('name')])
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Autocarat\Core\Supplier  $supplier
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Supplier $supplier)
    {
        return redirect()->route('suppliers.edit', ['supplier' => $supplier->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Autocarat\Core\Supplier  $supplier
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Supplier $supplier)
    {
        $this->authorize('update', $supplier);

        return view('suppliers.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Autocarat\Core\Supplier  $supplier
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Supplier $supplier)
    {
        $this->authorize('update', $supplier);

        $this->validate($request, [
            'name' => 'required|min:2',
            'note' => 'nullable'
        ]);

        $name = $supplier->name;

        $supplier->name = $request->input('name');
        $supplier->note = $request->input('note');

        $supplier->save();

        return redirect()->route('suppliers.index')
            ->with(
                'success',
                __('suppliers.update_success', ['name' => $name])
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Autocarat\Core\Supplier  $supplier
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Supplier $supplier)
    {
        $this->authorize('delete', $supplier);

        if ($supplier->supplies->isEmpty()) {
            //
            $name = $supplier->name;
            $supplier->delete();
            return redirect()->route('suppliers.index')
                ->with(
                    'success',
                    __('suppliers.delete_success', ['name' => $name])
                );
        }
        else {
            return redirect()->back()
                ->withErrors(
                    __(
                        'suppliers.cant_delete_has_supplies',
                        ['name' => $supplier->name]
                    )
                );
        }
    }
}
