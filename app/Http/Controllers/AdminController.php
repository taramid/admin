<?php

namespace App\Http\Controllers;

use Autocarat\Core\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Admin::class);

        $admins = Admin::all();

        return view('admins.index', [
            'admins' => $admins
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Admin::class);

        return view('admins.create', [
            'roles' => [
                [
                    'value' => Admin::ROLE_ADMIN,
                    'title' => __('admins.role_admin')
                ],
                [
                    'value' => Admin::ROLE_MANAGER,
                    'title' => __('admins.role_manager')
                ]
            ],
            'preselected' => Admin::ROLE_MANAGER
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Admin::class);

        $this->validate($request, [
            'name' => 'required|min:2',
            'role' => 'required|in:' . Admin::ROLE_ADMIN . ',' . Admin::ROLE_MANAGER,
            'note' => 'nullable',
            'email' => 'required|email|unique:admins'
        ]);

        $admin = new Admin();
        $admin->name = $request->input('name');
        $admin->role = $request->input('role');
        $admin->note = $request->input('note');
        $admin->email = $request->input('email');
        $admin->password = Hash::make('123123');

        $admin->save();

        return redirect()->route('admins.index')
            ->with(
                'success',
                __('admins.create_success', ['name' => $request->input('name')])
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Autocarat\Core\Admin $admin
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Admin $admin)
    {
        $this->authorize('view', $admin);

        return redirect()->route('admins.edit', ['admin' => $admin]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Autocarat\Core\Admin $admin
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Admin $admin)
    {
        $this->authorize('update', $admin);

        return view('admins.edit', [
            'admin' => $admin,
            'roles' => [
                [
                    'value' => Admin::ROLE_ADMIN,
                    'title' => __('admins.role_admin')
                ],
                [
                    'value' => Admin::ROLE_MANAGER,
                    'title' => __('admins.role_manager')
                ]
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Autocarat\Core\Admin $admin
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Admin $admin)
    {
        $this->authorize('update', $admin);

        $this->validate($request, [
            'name' => 'required|min:2',
            'role' => 'required|in:' . Admin::ROLE_ADMIN . ',' . Admin::ROLE_MANAGER,
            'note' => 'nullable',
            'email' => [
                'required',
                'email',
                Rule::unique('admins')->ignore($admin->id)
            ]
        ]);

        $name = $admin->name;

        $admin->name = $request->input('name');
        $admin->role = $request->input('role');
        $admin->note = $request->input('note');
        $admin->email = $request->input('email');

        $admin->save();

        return redirect()->route('admins.index')
            ->with(
                'success',
                __('admins.update_success', ['name' => $name])
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Autocarat\Core\Admin $admin
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Admin $admin)
    {
        $this->authorize('delete', $admin);

        // todo: create with earlier 'soft'-removed email address..

        $name = $admin->name;
        $admin->delete();
        return redirect()->route('admins.index')
            ->with(
                'success',
                __('admins.delete_success', ['name' => $name])
            );
    }
}
