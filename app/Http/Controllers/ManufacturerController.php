<?php

namespace App\Http\Controllers;

use Autocarat\Core\Manufacturer;
use Illuminate\Http\Request;

class ManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Manufacturer::class);

        return view('manufacturers.index', [
            'manufacturers' => Manufacturer::orderBy('name')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Manufacturer::class);

        return view('manufacturers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Manufacturer::class);

        $this->validate($request, [
            'name' => 'required|min:2',
            'note' => 'nullable'
        ]);

        $manufacturer = new Manufacturer();
        $manufacturer->name = $request->input('name');
        $manufacturer->note = $request->input('note');

        $manufacturer->save();

        return redirect()->route('manufacturers.index')
            ->with(
                'success',
                __('manufacturers.create_success', ['name' => $request->input('name')])
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Autocarat\Core\Manufacturer  $manufacturer
     * @return \Illuminate\Http\Response
     */
    public function show(Manufacturer $manufacturer)
    {
        return redirect()->route('manufacturers.edit', ['manufacturer' => $manufacturer->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Autocarat\Core\Manufacturer  $manufacturer
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Manufacturer $manufacturer)
    {
        $this->authorize('update', $manufacturer);

        return view('manufacturers.edit', compact('manufacturer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Autocarat\Core\Manufacturer  $manufacturer
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Manufacturer $manufacturer)
    {
        $this->authorize('update', $manufacturer);

        $this->validate($request, [
            'name' => 'required|min:2',
            'note' => 'nullable'
        ]);

        $name = $manufacturer->name;

        $manufacturer->name = $request->input('name');
        $manufacturer->note = $request->input('note');

        $manufacturer->save();

        return redirect()->route('manufacturers.index')
            ->with(
                'success',
                __('manufacturers.update_success', ['name' => $name])
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Autocarat\Core\Manufacturer  $manufacturer
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Manufacturer $manufacturer)
    {
        $this->authorize('delete', $manufacturer);

        if ($manufacturer->supplies->isEmpty()) {
            //
            $name = $manufacturer->name;
            $manufacturer->delete();
            return redirect()->route('manufacturers.index')
                ->with(
                    'success',
                    __('manufacturers.delete_success', ['name' => $name])
                );
        }
        else {
            return redirect()->back()
                ->withErrors(
                    __(
                        'manufacturers.cant_delete_has_supplies',
                        ['name' => $manufacturer->name]
                    )
                );
        }
    }
}
