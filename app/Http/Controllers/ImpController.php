<?php

namespace App\Http\Controllers;

use Autocarat\Core\Human;
use Autocarat\Core\Imp;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ImpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Imp::class);

        return view('imps.index', [
            'imps' => Imp::orderBy('code')->with(['humans:id,name,email'])->paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Imp::class);

        return view('imps.create', [
            'humans' => Human::orderBy('id')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Imp::class);

        $this->validate($request, [
            'code' => 'required|min:2|unique:imps',
            'note' => 'nullable'
        ]);

        $imp = new Imp();
        $imp->code= $request->input('code');
        $imp->note = $request->input('note');

        $imp->save();

        return redirect()->route('imps.index')
            ->with(
                'success',
                __('imps.create_success', ['code' => $request->input('code')])
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  \Autocarat\Core\Imp  $imp
     * @return \Illuminate\Http\Response
     */
    public function show(Imp $imp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Autocarat\Core\Imp  $imp
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Imp $imp)
    {
        $this->authorize('create', $imp);

        return view('imps.edit', [
                'imp' => $imp,
                'humans' => Human::select(['id','name','email'])->orderBy('id')->get()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Autocarat\Core\Imp  $imp
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Imp $imp)
    {
        $this->authorize('create', $imp);

        $this->validate($request, [
            'code' => [
                'required',
                'min:2',
                Rule::unique('imps')->ignore($imp->id)
            ],
            'note' => 'nullable'
        ]);

        $code = $imp->code;

        $imp->code = $request->input('code');
        $imp->note = $request->input('note');

        $imp->save();

        return redirect()->route('imps.index')
            ->with(
                'success',
                __('imps.update_success', ['code' => $code])
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Autocarat\Core\Imp  $imp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Imp $imp)
    {
        //
    }
}
