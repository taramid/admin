<?php

namespace App\Http\Middleware;

use Closure;
use \Autocarat\Core\Admin;

class EnsureAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->user()) || !$request->user()->isAdmin()) {
            abort(403);
        }

        return $next($request);
    }
}
