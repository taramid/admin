<?php

namespace App\Policies;

use Autocarat\Core\Admin;
use Autocarat\Core\Supplier;
use Illuminate\Auth\Access\HandlesAuthorization;

class SupplierPolicy
{
    use HandlesAuthorization;

    /**
     * Manage everything in one place.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function before(Admin $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can index suppliers.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function index(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can view the supplier.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Supplier  $supplier
     * @return mixed
     */
    public function view(Admin $user, Supplier $supplier)
    {
        //
    }

    /**
     * Determine whether the user can create suppliers.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the supplier.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Supplier  $supplier
     * @return mixed
     */
    public function update(Admin $user, Supplier $supplier)
    {
        //
    }

    /**
     * Determine whether the user can delete the supplier.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Supplier  $supplier
     * @return mixed
     */
    public function delete(Admin $user, Supplier $supplier)
    {
        //
    }

    /**
     * Determine whether the user can restore the supplier.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Supplier  $supplier
     * @return mixed
     */
    public function restore(Admin $user, Supplier $supplier)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the supplier.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Supplier  $supplier
     * @return mixed
     */
    public function forceDelete(Admin $user, Supplier $supplier)
    {
        //
    }
}
