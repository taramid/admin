<?php

namespace App\Policies;

use Autocarat\Core\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Manage everything in one place.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function before(Admin $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can index admins.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function index(Admin $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the admin.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Admin  $admin
     * @return mixed
     */
    public function view(Admin $user, Admin $admin)
    {
        //
    }

    /**
     * Determine whether the user can create admins.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the admin.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Admin  $admin
     * @return mixed
     */
    public function update(Admin $user, Admin $admin)
    {
        //
    }

    /**
     * Determine whether the user can delete the admin.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Admin  $admin
     * @return mixed
     */
    public function delete(Admin $user, Admin $admin)
    {
        //
    }

    /**
     * Determine whether the user can restore the admin.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Admin  $admin
     * @return mixed
     */
    public function restore(Admin $user, Admin $admin)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the admin.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Admin  $admin
     * @return mixed
     */
    public function forceDelete(Admin $user, Admin $admin)
    {
        //
    }
}
