<?php

namespace App\Policies;

use Autocarat\Core\Admin;
use Autocarat\Core\Minion;
use Illuminate\Auth\Access\HandlesAuthorization;

class MinionPolicy
{
    use HandlesAuthorization;

    /**
     * Manage everything in one place.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function before(Admin $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can index minions.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function index(Admin $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the minion.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Minion  $minion
     * @return mixed
     */
    public function view(Admin $user, Minion $minion)
    {
        //
    }

    /**
     * Determine whether the user can create minions.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the minion.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Minion  $minion
     * @return mixed
     */
    public function update(Admin $user, Minion $minion)
    {
        //
    }

    /**
     * Determine whether the user can delete the minion.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Minion  $minion
     * @return mixed
     */
    public function delete(Admin $user, Minion $minion)
    {
        //
    }

    /**
     * Determine whether the user can restore the minion.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Minion  $minion
     * @return mixed
     */
    public function restore(Admin $user, Minion $minion)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the minion.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Minion  $minion
     * @return mixed
     */
    public function forceDelete(Admin $user, Minion $minion)
    {
        //
    }
}
