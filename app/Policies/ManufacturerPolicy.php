<?php

namespace App\Policies;

use Autocarat\Core\Admin;
use Autocarat\Core\Manufacturer;
use Illuminate\Auth\Access\HandlesAuthorization;

class ManufacturerPolicy
{
    use HandlesAuthorization;

    /**
     * Manage everything in one place.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function before(Admin $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can index manufacturers.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function index(Admin $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the manufacturer.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Manufacturer  $manufacturer
     * @return mixed
     */
    public function view(Admin $user, Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Determine whether the user can create manufacturers.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the manufacturer.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Manufacturer  $manufacturer
     * @return mixed
     */
    public function update(Admin $user, Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Determine whether the user can delete the manufacturer.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Manufacturer  $manufacturer
     * @return mixed
     */
    public function delete(Admin $user, Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Determine whether the user can restore the manufacturer.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Manufacturer  $manufacturer
     * @return mixed
     */
    public function restore(Admin $user, Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the manufacturer.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Manufacturer  $manufacturer
     * @return mixed
     */
    public function forceDelete(Admin $user, Manufacturer $manufacturer)
    {
        //
    }
}
