<?php

namespace App\Policies;

use Autocarat\Core\Admin;
use Autocarat\Core\Supply;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\DB;

class SupplyPolicy
{
    use HandlesAuthorization;

    /**
     * Manage everything in one place.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function before(Admin $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can index supplies.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function index(Admin $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the supply.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Supply  $supply
     * @return mixed
     */
    public function view(Admin $user, Supply $supply)
    {
        // https://stackoverflow.com/questions/24555697/check-if-belongstomany-relation-exists-laravel

        return DB::table('admin_supply')

                ->where('admin_id', $user->id)
                ->where('supply_id', $supply->id)

//                 magic:
//                ->whereAdminId($user->id)
//                ->whereSupplyId($supply->id)

//                ->where([
//                    ['admin_id', '=', $user->id],
//                    ['supply_id', '=', $supply->id]
//                ])

                ->count() > 0;
    }

    /**
     * Determine whether the user can create supplies.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the supply.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Supply  $supply
     * @return mixed
     */
    public function update(Admin $user, Supply $supply)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the supply.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Supply  $supply
     * @return mixed
     */
    public function delete(Admin $user, Supply $supply)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can restore the supply.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Supply  $supply
     * @return mixed
     */
    public function restore(Admin $user, Supply $supply)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the supply.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Supply  $supply
     * @return mixed
     */
    public function forceDelete(Admin $user, Supply $supply)
    {
        return $user->isAdmin();
    }
}
