<?php

namespace App\Policies;

use Autocarat\Core\Admin;
use Autocarat\Core\Human;
use Illuminate\Auth\Access\HandlesAuthorization;

class HumanPolicy
{
    use HandlesAuthorization;

    /**
     * Manage everything in one place.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function before(Admin $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can index humans.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function index(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can view the human.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Human  $human
     * @return mixed
     */
    public function view(Admin $user, Human $human)
    {
        //
    }

    /**
     * Determine whether the user can create humans.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the human.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Human  $human
     * @return mixed
     */
    public function update(Admin $user, Human $human)
    {
        //
    }

    /**
     * Determine whether the user can delete the human.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Human  $human
     * @return mixed
     */
    public function delete(Admin $user, Human $human)
    {
        //
    }

    /**
     * Determine whether the user can restore the human.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Human  $human
     * @return mixed
     */
    public function restore(Admin $user, Human $human)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the human.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Human  $human
     * @return mixed
     */
    public function forceDelete(Admin $user, Human $human)
    {
        //
    }
}
