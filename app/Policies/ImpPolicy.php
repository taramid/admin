<?php

namespace App\Policies;

use Autocarat\Core\Admin;
use Autocarat\Core\Imp;
use Illuminate\Auth\Access\HandlesAuthorization;

class ImpPolicy
{
    use HandlesAuthorization;

    /**
     * Manage everything in one place.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function before(Admin $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can index imps.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function index(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can view the imp.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Imp  $imp
     * @return mixed
     */
    public function view(Admin $user, Imp $imp)
    {
        //
    }

    /**
     * Determine whether the user can create imps.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the imp.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Imp  $imp
     * @return mixed
     */
    public function update(Admin $user, Imp $imp)
    {
        //
    }

    /**
     * Determine whether the user can delete the imp.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Imp  $imp
     * @return mixed
     */
    public function delete(Admin $user, Imp $imp)
    {
        //
    }

    /**
     * Determine whether the user can restore the imp.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Imp  $imp
     * @return mixed
     */
    public function restore(Admin $user, Imp $imp)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the imp.
     *
     * @param  \Autocarat\Core\Admin  $user
     * @param  \Autocarat\Core\Imp  $imp
     * @return mixed
     */
    public function forceDelete(Admin $user, Imp $imp)
    {
        //
    }
}
