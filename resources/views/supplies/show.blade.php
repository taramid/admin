@extends('layouts.common')

@section('title', __('supplies.show_heading', ['name' => $supply->name]))

@section('heading', __('supplies.show_heading', ['name' => $supply->name]))

@section('main')

    @include('layouts.notification')

    <supply-show
            name="{{ $supply->name }}"
            @if (Auth::user()->can('update', $supply))
                href="{{ route('supplies.edit', ['supply' => $supply->id]) }}"
                :granted-to='@json($supply->admins)'
            @endif
    >
        <p>{{ $supply->supplier->name }}</p>
        <p>{{ $supply->manufacturer->name }}</p>
    </supply-show>

    <br>
    <button-link
            href="{{ route('catalog_imports.index', ['supply' => $supply->id]) }}"
            title="@lang('supplies.catalog_imports')"
    >
    </button-link>
    <button-link
            href="{{ route('discounts.index', ['supply' => $supply->id]) }}"
            title="@lang('supplies.discounts')"
    >
    </button-link>

@endsection
