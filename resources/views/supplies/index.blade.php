@extends('layouts.common')

@section('title', __('supplies.index_heading'))

@section('heading', __('supplies.index_heading'))

@section('main')

    @include('layouts.notification')

    <div class="justified-flex-container">
        @foreach ($supplies as $key => $supply)
            <supply
                    name="{{ $supply->name }}"
                    manufacturer="{{ $supply->manufacturer->name }}"
                    supplier="{{ $supply->supplier->name }}"
                    href="{{ route('supplies.show', ['supply' => $supply->id]) }}"
            ></supply>
        @endforeach
    </div>

    @if (
        Auth::check() &&
        Auth::user()->can('create', \Autocarat\Core\Supply::class)
    )
        <button-link
                color="is-primary"
                href="{{ route('supplies.create') }}"
                title="@lang('general.add_btn')"
        >
        </button-link>
    @endif

@endsection
