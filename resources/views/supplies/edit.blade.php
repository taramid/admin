@extends('layouts.common')

@section('title', __('supplies.edit_heading', ['name' => $supply->name]))

@section('heading', __('supplies.edit_heading', ['name' => $supply->name]))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('supplies.update', ['supply' => $supply->id]) }}">

        @csrf
        @method('PATCH')

        <text-input
                label="@lang('supplies.name')"
                id="name"
                value="{{ old('name') ?: $supply->name }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
        >
        </text-input>

        <dropdown
                label="@lang('supplies.supplier')"
                id="supplier"
                :options='@json($suppliers)'
                :preselected='{{ old('supplier') ?: $supply->supplier_id }}'
        >
        </dropdown>

        <dropdown
                label="@lang('supplies.manufacturer')"
                id="manufacturer"
                :options='@json($manufacturers)'
                :preselected='{{ old('manufacturer') ?: $supply->manufacturer_id }}'
        >
        </dropdown>

        <text-input
                label="@lang('supplies.note')"
                id="note"
                value="{{ old('note') ?: $supply->note }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.save_btn')"
                color="is-primary"
                cancel-href="{{ route('supplies.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

    <br>
    @include('components.delete_button', [
        'route' => 'supplies.destroy',
        'params' => [
            'id' => $supply->id
        ]
    ])

@endsection
