@extends('layouts.common')

@section('title', __('supplies.create_heading'))

@section('heading', __('supplies.create_heading'))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('supplies.store') }}">

        @csrf

        <text-input
                label="@lang('supplies.name')"
                id="name"
                value="{{ old('name') }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
                :focus="true"
        >
        </text-input>

        <dropdown
                label="@lang('supplies.supplier')"
                id="supplier"
                :options='@json($suppliers)'
                :preselected='{{ old('supplier') ?: 0 }}'
        >
        </dropdown>

        <dropdown
                label="@lang('supplies.manufacturer')"
                id="manufacturer"
                :options='@json($manufacturers)'
                :preselected='{{ old('manufacturer') ?: 0 }}'
        >
        </dropdown>

        <text-input
                label="@lang('supplies.note')"
                id="note"
                value="{{ old('note') }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.create_btn')"
                color="is-primary"
                cancel-href="{{ route('supplies.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

@endsection
