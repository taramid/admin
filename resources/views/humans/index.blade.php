@extends('layouts.common')

@section('title', __('humans.index_heading'))

@section('heading', __('humans.index_heading'))

@section('main')

    @include('layouts.notification')

    <div class="vertical-flex-container">
        @foreach ($humans as $key => $human)
            <human
                    name="{{ $human->name }}"
                    email="{{ $human->email }}"
                    note="{{ $human->note }}"
                    :imps='@json($human->imps)'
                    href="{{ route('humans.edit', ['human' => $human->id]) }}"
            ></human>
        @endforeach
    </div>

    {{ $humans->links('components.pagination') }}

    @if (
        Auth::check() &&
        Auth::user()->can('create', \Autocarat\Core\Human::class)
    )
        <button-link
                color="is-primary"
                href="{{ route('humans.create') }}"
                title="@lang('general.add_btn')"
        >
        </button-link>
    @endif

@endsection
