@extends('layouts.common')

@section('title', __('humans.create_heading'))

@section('heading', __('humans.create_heading'))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('humans.store') }}">

        @csrf

        <text-input
                label="@lang('humans.name')"
                id="name"
                value="{{ old('name') }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
                :focus="true"
        >
        </text-input>

        <text-input
                label="@lang('humans.email')"
                id="email"
                value="{{ old('email') }}"
                color="{{ $errors->has('email') ? 'is-danger' : '' }}"
        >
        </text-input>

        <text-input
                label="@lang('humans.note')"
                id="note"
                value="{{ old('note') }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.create_btn')"
                color="is-primary"
                cancel-href="{{ route('humans.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

@endsection
