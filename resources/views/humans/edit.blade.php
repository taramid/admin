@extends('layouts.common')

@section('title', __('humans.edit_heading', ['name' => $human->name]))

@section('heading', __('humans.edit_heading', ['name' => $human->name]))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('humans.update', ['human' => $human->id]) }}">

        @csrf
        @method('PATCH')

        <text-input
                label="@lang('humans.name')"
                id="name"
                value="{{ old('name') ?: $human->name }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
        >
        </text-input>

        <text-input
                label="@lang('humans.email')"
                id="email"
                value="{{ old('email') ?: $human->email }}"
                color="{{ $errors->has('email') ? 'is-danger' : '' }}"
        >
        </text-input>

        <text-input
                label="@lang('humans.note')"
                id="note"
                value="{{ old('note') ?: $human->note }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.save_btn')"
                color="is-primary"
                cancel-href="{{ route('humans.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

    <br>
    @include('components.delete_button', [
        'route' => 'humans.destroy',
        'params' => [
            'id' => $human->id
        ]
    ])

@endsection
