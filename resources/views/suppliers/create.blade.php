@extends('layouts.common')

@section('title', __('suppliers.create_heading'))

@section('heading', __('suppliers.create_heading'))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('suppliers.store') }}">

        @csrf

        <text-input
                label="@lang('suppliers.name')"
                id="name"
                value="{{ old('name') }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
                :focus="true"
        >
        </text-input>

        <text-input
                label="@lang('suppliers.note')"
                id="note"
                value="{{ old('note') }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.create_btn')"
                color="is-primary"
                cancel-href="{{ route('suppliers.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

@endsection
