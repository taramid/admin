@extends('layouts.common')

@section('title', __('suppliers.index_heading'))

@section('heading', __('suppliers.index_heading'))

@section('main')

    @include('layouts.notification')

    <div class="vertical-flex-container">
        @foreach ($suppliers as $key => $supplier)
            <supplier
                    name="{{ $supplier->name }}"
                    href="{{ route('suppliers.edit', ['supplier' => $supplier->id]) }}"
            ></supplier>
        @endforeach
    </div>

    @if (
        Auth::check() &&
        Auth::user()->can('create', \Autocarat\Core\Supplier::class)
    )
        <button-link
                color="is-primary"
                href="{{ route('suppliers.create') }}"
                title="@lang('general.add_btn')"
        >
        </button-link>
    @endif

@endsection
