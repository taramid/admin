@extends('layouts.common')

@section('title', __('suppliers.edit_heading', ['name' => $supplier->name]))

@section('heading', __('suppliers.edit_heading', ['name' => $supplier->name]))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('suppliers.update', ['supplier' => $supplier->id]) }}">

        @csrf
        @method('PATCH')

        <text-input
                label="@lang('suppliers.name')"
                id="name"
                value="{{ $supplier->name }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
        >
        </text-input>

        <text-input
                label="@lang('suppliers.note')"
                id="note"
                value="{{ $supplier->note }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.save_btn')"
                color="is-primary"
                cancel-href="{{ route('suppliers.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

    <br>
    @include('components.delete_button', [
        'route' => 'suppliers.destroy',
        'params' => [
            'id' => $supplier->id
        ]
    ])

@endsection
