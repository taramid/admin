@extends('layouts.common')

@section('title', __('imps.create_heading'))

@section('heading', __('imps.create_heading'))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('imps.store') }}">

        @csrf

        <text-input
                label="@lang('imps.code')"
                id="code"
                value="{{ old('code') }}"
                color="{{ $errors->has('code') ? 'is-danger' : '' }}"
                :focus="true"
        >
        </text-input>

        {{--<dropdown--}}
                {{--label="@lang('imps.humans')"--}}
                {{--id="human"--}}
                {{--:options='@json($humans)'--}}
                {{--:preselected='{{ old('human') ?: 0 }}'--}}
        {{-->--}}
        {{--</dropdown>--}}

        <text-input
                label="@lang('imps.note')"
                id="note"
                value="{{ old('note') }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.create_btn')"
                color="is-primary"
                cancel-href="{{ route('imps.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

@endsection
