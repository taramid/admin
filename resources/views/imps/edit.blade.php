@extends('layouts.common')

@section('title', __('imps.edit_heading', ['code' => $imp->code]))

@section('heading', __('imps.edit_heading', ['code' => $imp->code]))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('imps.update', ['imp' => $imp->id]) }}">

        @csrf
        @method('PATCH')

        <text-input
                label="@lang('imps.code')"
                id="code"
                value="{{ old('code') ?: $imp->code }}"
                color="{{ $errors->has('code') ? 'is-danger' : '' }}"
        >
        </text-input>

        {{--<dropdown--}}
                {{--label="@lang('imps.humans')"--}}
                {{--id="human"--}}
                {{--:options='@json($humans)'--}}
                {{--:preselected='{{ old('human') ?: $imps->manufacturer_id }}'--}}
        {{-->--}}
        {{--</dropdown>--}}

        <text-input
                label="@lang('imps.note')"
                id="note"
                value="{{ old('note') ?: $imp->note }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.save_btn')"
                color="is-primary"
                cancel-href="{{ route('imps.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

    <br>
    @include('components.delete_button', [
        'route' => 'imps.destroy',
        'params' => [
            'id' => $imp->id
        ]
    ])

@endsection
