@extends('layouts.common')

@section('title', __('imps.index_heading'))

@section('heading', __('imps.index_heading'))

@section('main')

    @include('layouts.notification')

    <div class="justified-flex-container">
        @foreach ($imps as $key => $imp)
            <imp
                    code="{{ $imp->code }}"
                    note="{{ $imp->note }}"
                    :humans='@json($imp->humans)'
                    href="{{ route('imps.edit', ['imp' => $imp->id]) }}"
            ></imp>
        @endforeach
    </div>

    {{ $imps->links('components.pagination') }}

    @if (
        Auth::check() &&
        Auth::user()->can('create', \Autocarat\Core\Imp::class)
    )
        <button-link
                color="is-primary"
                href="{{ route('imps.create') }}"
                title="@lang('general.add_btn')"
        >
        </button-link>
    @endif

@endsection
