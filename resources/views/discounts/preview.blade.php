@extends('layouts.common')

@section('title', __('discounts.preview_heading', ['supply' => $supply->name]))

@section('heading', __('discounts.preview_heading', ['supply' => $supply->name]))

@section('main')

    @include('layouts.notification')

    <div class="columns is-multiline is-mobile">
        <div class="column is-narrow">
            <table class="table is-bordered discounts">
                <thead>
                <tr>
                    <th rowspan="2">
                        @lang('terms.rg')
                    </th>
                    <th rowspan="2">
                        @lang('discounts.purchase')
                    </th>
                    <th colspan="{{ max(1, count($loyalties)) }}">
                        @lang('discounts.sale')
                    </th>
                </tr>
                <tr>
                    @foreach ($loyalties as $loyalty)
                        <th class="loyalty{{ !isset($loyalty['id']) || isset($loyalty['renameTo']) ? ' modified' : null }}">
                            {!!
                                $loyalty['renameTo'] ??
                                $loyalty['name'] ?? (
                                    isset($loyalty['id']) ? '<span class="loyalty-id">' . $loyalty['id'] . '</span>' : null
                                )
                            !!}
                        </th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach ($percentages as $rg)
                    <tr>
                        <td class="rg{{ !isset($rg['id']) ? ' modified' : null }}">
                            {{ $rg['name'] }}
                        </td>
                        <td class="purchase{{ isset($rg['percentTo']) ? ' modified' : null }}">
                            {{ $rg['percentTo'] ?? $rg['percent'] ?? null }}
                        </td>
                        @foreach($loyalties as $l => $loyalty)
                            @php
                                $classes = [];
                                if (isset($rg['sales'][$l]['percentTo'])) {
                                    $classes[] = 'modified';
                                }
                                if (0 < ($rg['sales'][$l]['percentTo'] ?? $rg['sales'][$l]['percent'] ?? 0)) {
                                    $classes[] = 'positive';
                                }
                            @endphp
                            <td{!! !empty($classes) ? ' class="' . implode(' ', $classes) . '"' : null !!}>
                                {{ $rg['sales'][$l]['percentTo'] ?? $rg['sales'][$l]['percent'] ?? null }}
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="column">
            <form method="POST"
                  action="{{ route('discounts.apply', ['supply' => $supply->id]) }}">

                @csrf

                <button-submit
                        id="submit"
                        title="@lang('general.apply_btn')"
                        color="is-primary"
                        :no-label="true"
                        cancel-href="{{ route('discounts.index', ['supply' => $supply->id]) }}"
                        cancel-title="@lang('general.cancel_btn')"
                >
                </button-submit>
            </form>
        </div>
    </div>

@endsection
