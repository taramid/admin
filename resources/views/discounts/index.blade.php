@extends('layouts.common')

@section('title', __('discounts.index_heading', ['supply' => $supply->name]))

@section('heading', __('discounts.index_heading', ['supply' => $supply->name]))

@section('main')

    @include('layouts.notification')

    <div class="columns is-multiline is-mobile">
        <div class="column is-narrow">
            <table class="table is-bordered discounts">
                <thead>
                <tr>
                    <th rowspan="2">
                        @lang('terms.rg')
                    </th>
                    <th rowspan="2">
                        @lang('discounts.purchase')
                    </th>
                    <th colspan="{{ max(1, count($supply->loyalties)) }}">
                        @lang('discounts.sale')
                    </th>
                </tr>
                <tr>
                    @foreach ($supply->loyalties as $loyalty)
                        <th class="loyalty">
                            {!! $loyalty->name ?? '<span class="loyalty-id">' . $loyalty->id . '</span>' !!}
                        </th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach ($matrix as $rg)
                    <tr>
                        <td class="rg">
                            {{ $rg['name'] }}
                        </td>
                        <td class="purchase">
                            {{ $rg['percent'] }}
                        </td>
                        @foreach ($rg['sales'] as $sale)
                            <td{{ isset($sale['percent']) && 0 < $sale['percent'] ? ' class=positive' : null }}>
                                {{ $sale['percent'] ?? null }}
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="column">
            <button-link
                    href="{{ route('discounts.export', ['supply' => $supply->id]) }}"
                    title="@lang('general.export_btn')"
                    fa="file-export"
            >
            </button-link>

            <form method="POST"
                  action="{{ route('discounts.upload', ['supply' => $supply->id]) }}"
                  enctype="multipart/form-data">

                @csrf

                <file
                        name="xlsx"
                        title="@lang('general.select_a_file')"
                        modifiers="is-boxed has-name"
                ></file>

                <br>
                <button-submit
                        id="submit"
                        title="@lang('general.upload_btn')"
                        color="is-primary"
                        :no-label="true"
                >
                </button-submit>
            </form>
        </div>
    </div>

@endsection
