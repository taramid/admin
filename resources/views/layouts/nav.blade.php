<nav class="navbar is-fixed-top is-light" role="navigation" aria-label="main navigation">
    <div class="container">

        <div class="navbar-brand">
            <a class="navbar-item" href="{{ url('/') }}">
                {{ config('app.name', 'ADMIN') }}
            </a>

            <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="#">1</a>
            </div>
            <div class="navbar-end">
                @include('layouts.nav_auth')
            </div>
        </div>

    </div>
</nav>
