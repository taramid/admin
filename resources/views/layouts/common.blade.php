@extends('layouts.app')

@section('content')
    <section class="container">
        <div class="columns">
            <div class="column is-one-fifth is-hidden-mobile">

                @include('layouts.menu')

            </div>
            <div class="column is-four-fifths main-content">

                <h3 class="title is-3">
                    @yield('heading')
                </h3>

                @yield('main')

            </div>
        </div>
    </section>
@endsection
