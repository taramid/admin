@guest
    <a class="navbar-item" href="{{ route('login') }}">
        {{ __('Login') }}
    </a>
@else
    <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
            {{ Auth::user()->name }}
        </a>
        <div class="navbar-dropdown is-boxed">
            <a class="navbar-item" href="{{ route('logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
            >
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">
                @csrf
            </form>
        </div>
    </div>
@endguest
