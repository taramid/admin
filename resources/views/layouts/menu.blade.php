<aside class="menu">
    <ul class="menu-list">
        @component('components.menu_item', ['name' => '/'])
            @lang('menu.dashboard')
        @endcomponent
    </ul>

    @if (
        Auth::user()->can('index', \Autocarat\Core\Manufacturer::class) ||
        Auth::user()->can('index', \Autocarat\Core\Supplier::class) ||
        Auth::user()->can('index', \Autocarat\Core\Supply::class)
    )
        <p class="menu-label">
            от это всё
        </p>
        <ul class="menu-list">
            @can('index', \Autocarat\Core\Manufacturer::class)
                @component('components.menu_item', ['name' => 'manufacturers'])
                    @lang('menu.manufacturers')
                @endcomponent
            @endcan
            @can('index', \Autocarat\Core\Supplier::class)
                @component('components.menu_item', ['name' => 'suppliers'])
                    @lang('menu.suppliers')
                @endcomponent
            @endcan
            @can('index', \Autocarat\Core\Supply::class)
                @component('components.menu_item', ['name' => 'supplies'])
                    @lang('menu.supplies')
                @endcomponent
            @endcan
        </ul>
    @endif

    @if (
        Auth::user()->can('index', \Autocarat\Core\Human::class) ||
        Auth::user()->can('index', \Autocarat\Core\Imp::class)
    )
        <p class="menu-label">
            @lang('menu.label_clients')
        </p>
        <ul class="menu-list">
            @can('index', \Autocarat\Core\Human::class)
                @component('components.menu_item', ['name' => 'humans'])
                    @lang('menu.humans')
                @endcomponent
            @endcan
            @can('index', \Autocarat\Core\Imp::class)
                @component('components.menu_item', ['name' => 'imps'])
                    @lang('menu.imps')
                @endcomponent
            @endcan
            {{--<li><a href="{{ url('human_imp') }}">@lang('menu.human_imp')</a></li>--}}
        </ul>
    @endif

    @if (
        Auth::user()->can('index', \Autocarat\Core\Admin::class) ||
        Auth::user()->can('index', \Autocarat\Core\Minion::class)
    )
        <p class="menu-label">
            @lang('menu.label_admins')
        </p>
        <ul class="menu-list">
            @can('index', \Autocarat\Core\Admin::class)
                @component('components.menu_item', ['name' => 'admins'])
                    @lang('menu.admins')
                @endcomponent
            @endcan
            @can('index', \Autocarat\Core\Minion::class)
                @component('components.menu_item', ['name' => 'minions'])
                    @lang('menu.minions')
                @endcomponent
            @endcan
        </ul>
    @endif

    <p class="menu-label">
        @lang('menu.label_orders')
    </p>
    <ul class="menu-list">
        {{--<li><a href="{{ url('admins') }}">@lang('menu.admin_list')</a></li>--}}
    </ul>

    <p class="menu-label">
        @lang('menu.label_waren')
    </p>
    <ul class="menu-list">
        {{--<li><a href="{{ url('admins') }}">@lang('menu.admin_list')</a></li>--}}
    </ul>

    <p class="menu-label">
        @lang('menu.label_bestand')
    </p>
    <ul class="menu-list">
        {{--<li><a href="{{ url('admins') }}">@lang('menu.admin_list')</a></li>--}}
    </ul>
</aside>
