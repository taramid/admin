@if(session()->has('success'))
    <notification color="is-success">
        {!! session()->get('success') !!}
    </notification>
@endif

@if(session()->has('note'))
    <notification color="is-info">
        {!! session()->get('note') !!}
    </notification>
@endif

@if ($errors->any())
    <notification color="is-warning">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{!! $error  !!}</li>
            @endforeach
        </ul>
    </notification>
@endif
