<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="has-navbar-fixed-top">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- csrf token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name', 'Laravel'))</title>

    {{-- scripts --}}
    {{--<script src="{{ asset(mix('js/manifest.js')) }}" defer></script>--}}
    {{--<script src="{{ asset(mix('js/vendor.js')) }}" defer></script>--}}
    <script src="{{ asset(mix('js/app.js')) }}" defer></script>

    {{-- fonts --}}
    <link href="https://fonts.gstatic.com" rel="dns-prefetch">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    {{-- styles --}}
    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
</head>
<body>
<div id="app">

    @include('layouts.nav')

    @yield('content')

</div>
</body>
</html>
