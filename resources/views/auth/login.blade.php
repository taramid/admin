@extends('layouts.app')

@section('content')

    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <div class="columns is-centered">
                    <div class="card">
                        <div class="card-content login-form">
                            <h1 class="title">
                                @lang('auth.login')
                            </h1>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="control has-icons-left">
                                    <input
                                            type="email"
                                            name="email"
                                            class="input{{ $errors->has('email') ? ' is-danger' : '' }}"
                                            required
                                            autofocus
                                            value="{{ old('email') }}"
                                            placeholder="@lang('auth.email')"
                                    >
                                    <span class="icon is-small is-left">
                                        <fa icon="envelope"></fa>
                                    </span>
                                </div>
                                <div class="control has-icons-left">
                                    <input
                                            type="password"
                                            name="password"
                                            class="input{{ $errors->has('password') ? ' is-danger' : '' }}"
                                            required
                                            value="{{ old('password') }}"
                                            placeholder="@lang('auth.password')"
                                    >
                                    <span class="icon is-small is-left">
                                        <fa icon="lock"></fa>
                                    </span>
                                </div>
                                <div class="control">
                                    <label class="checkbox">
                                        <input
                                                type="checkbox"
                                                name="remember"
                                                {{ old('remember') ? 'checked' : '' }}
                                        >
                                        @lang('auth.remember')
                                    </label>
                                </div>
                                <div class="control">
                                    <button class="button is-primary is-fullwidth">
                                        @lang('auth.login_btn')
                                    </button>
                                </div>
                                <div class="control">
                                    <a href="{{ route('password.request') }}">
                                        @lang('auth.forgot_password')
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
