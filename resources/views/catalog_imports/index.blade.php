@extends('layouts.common')

@section('title', __('catalog_imports.index_heading', ['supply' => $supply->name]))

@section('heading', __('catalog_imports.index_heading', ['supply' => $supply->name]))

@section('main')

    @include('layouts.notification')

    <form method="POST"
          action="{{ route('catalog_imports.upload', ['supply' => $supply->id]) }}"
          enctype="multipart/form-data">

        @csrf

        <file
                name="csv[]"
                title="@lang('general.select_files')"
                modifiers="is-boxed has-name"
                multiple
        ></file>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.upload_btn')"
                color="is-primary"
                :no-label="true"
        >
        </button-submit>

    </form>

    <hr>

    <div class="vertical-flex-container">
        @foreach ($catalog_imports as $key => $import)
            <catalog-import
                    id="{{ $import->id }}"
                    created-at="{{ $import->created_at }}"
                    preview-url="{{ route('catalog_imports.preview', [
                        'supply' => $import->supply_id,
                        'catalog_import' => $import->id
                    ]) }}"
            >
                <template slot="apply-btn">
                    <form method="POST"
                          action="{{ route('catalog_imports.apply', [
                              'supply' => $import->supply_id, 'import' => $import->id
                          ]) }}"
                          >

                        @csrf

                        <button type="submit" class="button is-small is-link">
                            <fa icon="check" size="lg"></fa>
                        </button>
                    </form>
                </template>

                <template slot="delete-btn">
                    <form method="POST"
                          action="{{ route('catalog_imports.destroy', [
                              'supply' => $import->supply_id, 'import' => $import->id
                          ]) }}"
                          class="delete-btn-form">

                        @csrf

                        {{ method_field('DELETE') }}

                        <button type="submit" class="button is-small is-danger">
                            <fa icon="trash-alt" size="lg"></fa>
                        </button>
                    </form>
                </template>

            </catalog-import>
        @endforeach
    </div>

@endsection
