@if ($paginator->hasPages())
    <br>
    <nav class="pagination" role="navigation" aria-label="pagination">

        {{-- previous --}}
        @if ($paginator->onFirstPage())
            <a class="pagination-previous" aria-label="@lang('pagination.previous')" disabled>
                <fa icon="chevron-left"></fa>
            </a>
        @else
            <a
                    class="pagination-previous"
                    href="{{ $paginator->previousPageUrl() }}"
                    rel="prev"
                    aria-label="@lang('pagination.previous')"
            >
                <fa icon="chevron-left"></fa>
            </a>
        @endif

        <ul class="pagination-list">
            @foreach ($elements as $element)

                {{-- separator --}}
                @if (is_string($element))
                    <li>
                        <span class="pagination-ellipsis">&hellip;</span>
                    </li>
                @endif

                {{-- pages --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        <li>
                            <a
                                @if ($page == $paginator->currentPage())
                                    class="pagination-link is-current"
                                    aria-current="page"
                                @else
                                    class="pagination-link"
                                    href="{{ $url }}"
                                @endif

                                aria-label="@lang('pagination.page', ['page' => $page])"
                            >
                                {{ $page }}
                            </a>
                        </li>
                    @endforeach
                @endif

            @endforeach
        </ul>

        {{-- next --}}
        @if ($paginator->hasMorePages())
            <a
                    class="pagination-next"
                    href="{{ $paginator->nextPageUrl() }}"
                    rel="next"
                    aria-label="@lang('pagination.next')"
            >
                <fa icon="chevron-right"></fa>
            </a>
        @else
            <a class="pagination-next" aria-label="@lang('pagination.next')" disabled>
                <fa icon="chevron-right"></fa>
            </a>
        @endif

    </nav>
@endif
