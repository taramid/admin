<form method="POST" action="{{ route($route, $params) }}">
    @csrf
    @method('DELETE')
    <button-delete title="@lang('general.delete_btn')"></button-delete>
</form>
