<li>
    <a href="{{ url($name) }}" {!! request()->is($name.'*') ? 'class="is-active"' : '' !!} >
        {{ $slot }}
    </a>
</li>
