@extends('layouts.common')

@section('title', __('minions.create_heading'))

@section('heading', __('minions.create_heading'))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('minions.store') }}">

        @csrf

        <text-input
                label="@lang('minions.name')"
                id="name"
                value="{{ old('name') }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
                :focus="true"
        >
        </text-input>

        <text-input
                label="@lang('minions.code')"
                id="code"
                value="{{ old('code') }}"
                color="{{ $errors->has('code') ? 'is-danger' : '' }}"
        >
        </text-input>

        <text-input
                label="@lang('minions.note')"
                id="note"
                value="{{ old('note') }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.create_btn')"
                color="is-primary"
                cancel-href="{{ route('minions.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

@endsection
