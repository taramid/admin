@extends('layouts.common')

@section('title', __('minions.index_heading'))

@section('heading', __('minions.index_heading'))

@section('main')

    @include('layouts.notification')

    <div class="vertical-flex-container">
        @foreach ($minions as $key => $minion)
            <minion
                    name="{{ $minion->name }}"
                    code="{{ $minion->code }}"
                    note="{{ $minion->note }}"
                    href="{{ route('minions.edit', ['minion' => $minion->id]) }}"
            ></minion>
        @endforeach
    </div>

    @if (
        Auth::check() &&
        Auth::user()->can('create', \Autocarat\Core\Minion::class)
    )
        <button-link
                color="is-primary"
                href="{{ route('minions.create') }}"
                title="@lang('general.add_btn')"
        >
        </button-link>
    @endif

@endsection
