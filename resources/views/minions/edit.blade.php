@extends('layouts.common')

@section('title', __('minions.edit_heading', ['name' => $minion->name]))

@section('heading', __('minions.edit_heading', ['name' => $minion->name]))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('minions.update', ['supplier' => $minion->id]) }}">

        @csrf
        @method('PATCH')

        <text-input
                label="@lang('minions.name')"
                id="name"
                value="{{ old('name') ?: $minion->name }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
        >
        </text-input>

        <text-input
                label="@lang('minions.code')"
                id="code"
                value="{{ old('code') ?: $minion->code }}"
                color="{{ $errors->has('code') ? 'is-danger' : '' }}"
        >
        </text-input>

        <text-input
                label="@lang('minions.note')"
                id="note"
                value="{{ old('note') ?: $minion->note }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.save_btn')"
                color="is-primary"
                cancel-href="{{ route('minions.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

    <br>
    @include('components.delete_button', [
        'route' => 'minions.destroy',
        'params' => [
            'id' => $minion->id
        ]
    ])

@endsection
