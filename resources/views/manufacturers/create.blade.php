@extends('layouts.common')

@section('title', __('manufacturers.create_heading'))

@section('heading', __('manufacturers.create_heading'))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('manufacturers.store') }}">

        @csrf

        <text-input
                label="@lang('manufacturers.name')"
                id="name"
                value="{{ old('name') }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
                placeholder="FERRARI"
                :focus="true"
        >
        </text-input>

        <text-input
                label="@lang('manufacturers.note')"
                id="note"
                value="{{ old('note') }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.create_btn')"
                color="is-primary"
                cancel-href="{{ route('manufacturers.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

@endsection
