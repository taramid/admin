@extends('layouts.common')

@section('title', __('manufacturers.edit_heading', ['name' => $manufacturer->name]))

@section('heading', __('manufacturers.edit_heading', ['name' => $manufacturer->name]))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('manufacturers.update', ['manufacturer' => $manufacturer->id]) }}">

        @csrf
        @method('PATCH')

        <text-input
                label="@lang('manufacturers.name')"
                id="name"
                value="{{ $manufacturer->name }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
        >
        </text-input>

        <text-input
                label="@lang('manufacturers.note')"
                id="note"
                value="{{ $manufacturer->note }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.save_btn')"
                color="is-primary"
                cancel-href="{{ route('manufacturers.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

    <br>
    @include('components.delete_button', [
        'route' => 'manufacturers.destroy',
        'params' => [
            'id' => $manufacturer->id
        ]
    ])

@endsection
