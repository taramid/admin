@extends('layouts.common')

@section('title', __('manufacturers.index_heading'))

@section('heading', __('manufacturers.index_heading'))

@section('main')

    @include('layouts.notification')

    <div class="justified-flex-container">
        @foreach ($manufacturers as $key => $manufacturer)
            <manufacturer
                    name="{{ $manufacturer->name }}"
                    href="{{ route('manufacturers.edit', ['manufacturer' => $manufacturer->id]) }}"
            ></manufacturer>
        @endforeach
    </div>

    @if (
        Auth::check() &&
        Auth::user()->can('create', \Autocarat\Core\Manufacturer::class)
    )
        <button-link
                color="is-primary"
                href="{{ route('manufacturers.create') }}"
                title="@lang('general.add_btn')"
        >
        </button-link>
    @endif

@endsection
