@extends('layouts.common')

@section('title', __('admins.create_heading'))

@section('heading', __('admins.create_heading'))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('admins.store') }}">

        @csrf

        <text-input
                label="@lang('admins.name')"
                id="name"
                value="{{ old('name') }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
        >
        </text-input>

        <dropdown
                label="@lang('admins.role')"
                id="role"
                :options='@json($roles)'
                :preselected="{{ old('role') ?: $preselected }}"
        >
        </dropdown>

        <text-input
                label="@lang('admins.note')"
                id="note"
                value="{{ old('note') }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <text-input
                label="@lang('admins.email')"
                id="email"
                value="{{ old('email') }}"
                color="{{ $errors->has('email') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.create_btn')"
                color="is-primary"
                cancel-href="{{ route('admins.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

@endsection
