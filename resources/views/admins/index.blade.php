@extends('layouts.common')

@section('title', __('admins.index_heading'))

@section('heading', __('admins.index_heading'))

@section('main')

    @include('layouts.notification')

    <table class="table is-bordered is-striped">
        <thead>
        <tr>
            <th></th>
            <th>@lang('admins.name')</th>
            <th>@lang('admins.role')</th>
            <th>@lang('admins.note')</th>
            <th>@lang('admins.email')</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($admins as $key => $admin)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>
                    {{ $admin->name }}
                </td>
                <td class="has-text-centered">
                    @if (Autocarat\Core\Admin::ROLE_ADMIN === $admin->role)
                        <fa icon="star"></fa>
                    @endif
                </td>
                <td>{{ $admin->note }}</td>
                <td>{{ $admin->email }}</td>
                <td>
                    <a href="{{ route('admins.edit', ['admin' => $admin->id]) }}">
                        <fa icon="cog"></fa>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if (
        Auth::check() &&
        Auth::user()->can('create', \Autocarat\Core\Admin::class)
    )
        <button-link
                color="is-primary"
                href="{{ route('admins.create') }}"
                title="@lang('general.add_btn')"
        >
        </button-link>
    @endif

@endsection
