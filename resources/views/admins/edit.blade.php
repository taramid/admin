@extends('layouts.common')

@section('title', __('admins.edit_heading', ['name' => $admin->name]))

@section('heading', __('admins.edit_heading', ['name' => $admin->name]))

@section('main')

    @include('layouts.notification')

    <form method="POST" action="{{ route('admins.update', ['admin' => $admin->id]) }}">

        @csrf
        @method('PATCH')

        <text-input
                label="@lang('admins.name')"
                id="name"
                value="{{ old('name') ?: $admin->name }}"
                color="{{ $errors->has('name') ? 'is-danger' : '' }}"
        >
        </text-input>

        <dropdown
                label="@lang('admins.role')"
                id="role"
                :options='@json($roles)'
                :preselected="{{ old('role') ?: $admin->role }}"
        >
        </dropdown>

        <text-input
                label="@lang('admins.note')"
                id="note"
                value="{{ old('note') ?: $admin->note }}"
                color="{{ $errors->has('note') ? 'is-danger' : '' }}"
        >
        </text-input>

        <text-input
                label="@lang('admins.email')"
                id="email"
                value="{{ old('email') ?: $admin->email }}"
                color="{{ $errors->has('email') ? 'is-danger' : '' }}"
        >
        </text-input>

        <br>
        <button-submit
                id="submit"
                title="@lang('general.save_btn')"
                color="is-primary"
                cancel-href="{{ route('admins.index') }}"
                cancel-title="@lang('general.cancel_btn')"
        >
        </button-submit>

    </form>

    <br>
    @include('components.delete_button', [
        'route' => 'admins.destroy',
        'params' => [
            'id' => $admin->id
        ]
    ])

@endsection
