<?php

return [


    'index_heading' => 'Люди',

    'create_success' => 'Создан человек <strong>:name</strong>',
    'update_success' => '<strong>:name</strong> изменён',
    'delete_success' => '<strong>:name</strong> удалён',

    'name' => 'Имя',
    'email' => 'Эл. адрес',
    'note' => 'Примечание',


    'create_heading' => 'Создать человека',



    'edit_heading' => 'Человек :name',

//    'cant_delete_has__' => 'Нельзя удалить человека <strong>:name</strong>, т.к...'

];
