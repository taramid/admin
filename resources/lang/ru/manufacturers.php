<?php

return [


    'index_heading' => 'Производители',

    'create_success' => 'Создан производитель <strong>:name</strong>',
    'update_success' => '<strong>:name</strong> изменён',
    'delete_success' => '<strong>:name</strong> удалён',

    'name' => 'Название',
    'note' => 'Примечание',


    'create_heading' => 'Создать производителя',



    'edit_heading' => 'Производитель :name',

    'cant_delete_has_supplies' => 'Нельзя удалить производителя <strong>:name</strong>, т.к. у него есть канал(ы) поставки'

];
