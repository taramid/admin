<?php

return [


    'index_heading' => 'Поставщики',

    'create_success' => 'Создан поставщик <strong>:name</strong>',
    'update_success' => '<strong>:name</strong> изменён',
    'delete_success' => '<strong>:name</strong> удалён',

    'name' => 'Название',
    'note' => 'Примечание',


    'create_heading' => 'Создать поставщика',



    'edit_heading' => 'Поставщик :name',

    'cant_delete_has_supplies' => 'Нельзя удалить поставщика <strong>:name</strong>, т.к. у него есть канал(ы) поставки'

];
