<?php

return [

    'add_btn' => 'Добавить',

    'create_btn' => 'Создать',
    'save_btn' => 'Сохранить',

    'download_btn' => 'Скачать',

    'upload_empty' => 'файл не загружен',

    'select_a_file' => 'Выберите файл',
    'select_files' => 'Выберите файл(ы)',
    'upload_btn' => 'Загрузить',

    'cancel_btn' => 'Отмена',

    'delete_btn' => 'Удалить',

    'import_btn' => 'Импорт',
    'export_btn' => 'Экспорт',

    'apply_btn' => 'Применить',

];
