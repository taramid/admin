<?php

return [


    'index_heading' => 'Администраторы',

    'create_success' => 'Создан администратор <strong>:name</strong>',
    'update_success' => 'Администратор <strong>:name</strong> изменён',
    'delete_success' => 'Администратор <strong>:name</strong> удалён',

    'name' => 'Имя',
    'role' => 'Роль',
    'note' => 'Примечание',
    'email' => 'Эл. адрес',

    'role_admin' => 'администратор',
    'role_manager' => 'менеджер',



    'create_heading' => 'Создать администратора',
    'edit_heading' => 'Администратор :name',




];
