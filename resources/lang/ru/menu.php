<?php

return [


    'dashboard' => 'Обзор', // "Сводка" "Как Дела?" https://toster.ru/q/22992

    'manufacturers' => 'Производители',
    'suppliers' => 'Поставщики',
    'supplies' => 'Каналы поставки',

    'label_clients' => 'Клиенты',
    'humans' => 'Люди',
    'imps' => 'Коды',
    'human_imp' => 'Доступ',

    'label_admins' => 'Персонал',
    'admins' => 'Администраторы',
    'minions' => 'Работники',

    'label_orders' => 'Заказы',

    'label_waren' => 'Склад',

    'label_bestand' => 'Залежи',
];
