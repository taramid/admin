<?php

return [


    'index_heading' => 'Скидки для :supply',

    'purchase' => 'закупка, %',
    'sale' => 'продажа, %',

    'filename' => ':id :supply Rabatte',

    'upload_has_no_changes' => 'без изменений',
    'upload_note_wrong_supply_id' => 'возможно, вы пытаетесь загрузить скидки от другого поставщика',

    'apply_success' => 'скидки изменены',


    'preview_heading' => 'Изменение скидок для :supply',

];
