<?php

return [


    'index_heading' => 'Каналы поставки',

    'create_success' => 'Создан канал поставки <strong>:name</strong>',
    'update_success' => '<strong>:name</strong> изменён',
    'delete_success' => '<strong>:name</strong> удалён',

    'name' => 'Название',
    'manufacturer' => 'Производитель',
    'supplier' => 'Поставщик',
    'note' => 'Примечание',


    'create_heading' => 'Создать канал поставки',



    'show_heading' => 'Канал поставки :name',
    'catalog_imports' => 'Прайсы',
    'discounts' => 'Скидки',


    'edit_heading' => 'Канал поставки :name',

    'cant_delete_has__' => 'Нельзя удалить канал поставки<strong>:name</strong>, т.к...'

];
