<?php

return [


    'index_heading' => 'Работники',

    'create_success' => 'Создан работник <strong>:name</strong>',
    'update_success' => '<strong>:name</strong> изменён',
    'delete_success' => '<strong>:name</strong> удалён',

    'name' => 'Имя',
    'code' => 'Код',
    'note' => 'Примечание',


    'create_heading' => 'Создать работника',



    'edit_heading' => 'Работник :name',

//    'cant_delete_has__' => 'Нельзя удалить канал поставки<strong>:name</strong>, т.к...'

];
