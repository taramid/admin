<?php

return [


    'index_heading' => 'Коды',

    'create_success' => 'Создан код <strong>:code</strong>',
    'update_success' => '<strong>:code</strong> изменён',
    'delete_success' => '<strong>:code</strong> удалён',

    'code' => 'Код',
    'note' => 'Примечание',


    'create_heading' => 'Создать код',



    'edit_heading' => 'код :code',

//    'cant_delete_has__' => 'Нельзя удалить человека <strong>:name</strong>, т.к...'

];
