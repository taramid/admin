<?php

return [


    'index_heading' => 'Загрузка прайсов для :supply',

    'upload_success' => 'Файл загружен (#<strong>:id</strong>):<br><strong>:files</strong>|Файлы загружены (#<strong>:id</strong>):<br><strong>:files</strong>',
    'upload_note' => 'Содержимое файла будет автоматически прочитано и доступно для просмотра/подтверждения|Содержимое файлов будет автоматически прочитано и доступно для просмотра/подтверждения',

    'delete_success' => 'Прайс #<strong>:id</strong> удалён',
    'cant_delete_now' => 'Сейчас нельзя удалить прайс #<strong>:id</strong>',

    'apply_success' => 'Подтверждена активация прайса #<strong>:id</strong>',
    'cant_apply' => 'Сейчас нельзя активировать прайс #<strong>:id</strong>',




    'create_heading' => 'Загрузить прайс для :supply',


];
