<?php

return [

    'previous' => 'Назад',
    'next' => 'Вперёд',

    'page' => 'Страница :page',

];
