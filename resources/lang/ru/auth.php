<?php

return [

    'login' => 'Вход',

    'email' => 'эл. адрес',
    'password' => 'пароль',

    'remember' => 'Запомнить',
    'forgot_password' => 'забыли пароль?',

    'login_btn' => 'Войти',

];
