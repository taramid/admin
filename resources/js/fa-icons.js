import Vue from 'vue';

import {library} from '@fortawesome/fontawesome-svg-core';
import {
    faCheck,
    faChevronLeft,
    faChevronRight,
    faCog,
    faDownload,
    faEdit,
    faEnvelope,
    faFileExport,
    faEye,
    faFileImport,
    faLock,
    faStar,
    faTrashAlt,
    faUpload,
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';

Vue.component('fa', FontAwesomeIcon);

library.add(
    faCheck,
    faChevronLeft,
    faChevronRight,
    faCog,
    faDownload,
    faEdit,
    faEnvelope,
    faFileExport,
    faEye,
    faFileImport,
    faLock,
    faStar,
    faTrashAlt,
    faUpload,
);
