import './bootstrap';
import './fa-icons.js';

import Vue from 'vue';
window.Vue = Vue;


import TextInput from './elements/TextInput';
Vue.component('text-input', TextInput);

import Dropdown from './elements/Dropdown';
Vue.component('dropdown', Dropdown);

import Checkbox from './elements/Checkbox';
Vue.component('checkbox', Checkbox);

import ButtonLink from './elements/ButtonLink';
Vue.component('button-link', ButtonLink);

import ButtonSubmit from './elements/ButtonSubmit';
Vue.component('button-submit', ButtonSubmit);

import ButtonDelete from './elements/ButtonDelete';
Vue.component('button-delete', ButtonDelete);

import Notification from './elements/Notification';
Vue.component('notification', Notification);

import File from './elements/File';
Vue.component('file', File);


import Manufacturer from './components/Manufacturer.vue';
Vue.component('manufacturer', Manufacturer);

import Supplier from './components/Supplier.vue';
Vue.component('supplier', Supplier);

import Supply from './components/Supply.vue';
Vue.component('supply', Supply);

import SupplyShow from './components/SupplyShow.vue';
Vue.component('supply-show', SupplyShow);

import Minion from './components/Minion.vue';
Vue.component('minion', Minion);

import Human from './components/Human.vue';
Vue.component('human', Human);

import Imp from './components/Imp.vue';
Vue.component('imp', Imp);

import CatalogImport from './components/CatalogImport.vue';
Vue.component('catalog-import', CatalogImport);

const app = new Vue({
    el: '#app'
});
