"use strict";

export default {

    centAsEuro: function(cent_) {
        let cent = parseInt(cent_);
        return cent ? (.01 * cent).toFixed(2) : null;
    },

    gramAsKg: function(g_) {
        let g = parseInt(g_);
        return g ? (.01 * g).toFixed(2) : null;
    },

};
