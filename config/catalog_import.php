<?php

return [

    'part_number_min_length' => env('CATALOG_IMPORT_PART_NUMBER_MIN_LENGTH', 5),
    'chunk_size' => env('CATALOG_IMPORT_CHUNK_SIZE', 2000),

];
